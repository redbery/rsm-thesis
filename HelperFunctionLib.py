import json, numpy, scipy

def AverageLists(ListOfLists) -> list:
    '''
    Average each element across multiple lists.

    Args:
        ListOfLists (list): List of lists for which to average each element.

    Returns:
        list: The list with averages of each element
    '''    
    arrays = [numpy.array(x) for x in ListOfLists]
    return [numpy.mean(k) for k in zip(*arrays)]

def StandardDeviationLists(ListOfLists) -> list:
    '''
    Calculate Std for each element across multiple lists.

    Args:
        ListOfLists (list): List of lists for which to calculate each element.

    Returns:
        list: The list with std of each element
    '''    
    arrays = [numpy.array(x) for x in ListOfLists]
    return [numpy.std(k) for k in zip(*arrays)]

def CachedBetaPPF(Percentile, Successes, Failures, Cache = None) -> float:
    '''
    Calculate the expected probability of the arm at a specified percentile on the distribution

    Args:
        Percentile (float): The percentile we want information about as a 0-1 value
        Successes (int): Amount of successes so far
        Failures (int): Amount of failures so far

    Returns:
        float: The calculated mean at percentile
    '''    
    if Cache:
        print("Copied cache from input")
        CachedBetaPPF.Cache = Cache
        
    if not hasattr(CachedBetaPPF, "Cache"):
        with open('CachedBetaPPF.json', 'r') as f:
            CachedBetaPPF.Cache = json.load(f)
            print("Loaded cache for PPF")
    
    try:
        return CachedBetaPPF.Cache[str(Percentile)+"-"+str(Successes)+"-"+str(Failures)]
    except:
        #print(str(Percentile)+"-"+str(Successes)+"-"+str(Failures))
        return scipy.stats.beta.ppf(Percentile, Successes+1, Failures+1)