import sys
from datetime import datetime

@staticmethod
def LoggerPrefix(SimulatorNumber=None, TeamNumber=None) -> str:
    '''
    Create a prefix to write into the logger depending on who is going to print.

    Args:
        SimulatorNumber (_type_): The number of the simulator the call comes from.
        TeamNumber (_type_): The number of the team the call comes from.

    Returns:
        str: A string to write at the start of the printed message.
    '''    
    Prefix = "[" + str(datetime.now()) + "] "
    if SimulatorNumber:
        Prefix += "Simulator " + str(SimulatorNumber) + " "
    if TeamNumber:
        Prefix += "Team " + str(TeamNumber) + " "
    Prefix += "- "
    return Prefix

class Logger(object):
    '''
    Custom logger which outputs to a file and the console at the same time. Allows for easy creation of an audit trail.
    '''    
    
    def __init__(self) -> None:
        self.OutputFile = open("./Logs/" + str(datetime.now()).replace(":", "_").replace(" ", "_"), "w")
        self.OriginalOut = sys.stdout
        self.Outputs = (self.OutputFile, self.OriginalOut)
        sys.stdout = self
        print("Log created at: " + str(datetime.now()))
        
    def __del__(self) -> None:
        self.OutputFile.close()
        sys.stdout = self.OriginalOut
        
    def write(self, obj) -> None:
        #self.OutputFile.write(obj)
        # Use the below code to also get an output to console, this is MUCH slower than file only
        for Output in self.Outputs:
            Output.write(obj)
            Output.flush()
        
    def flush(self) -> None:
        #self.OutputFile.flush()
        # Use the below code to also get an output to console, this is MUCH slower than file only
        for Output in self.Outputs:
            Output.flush()