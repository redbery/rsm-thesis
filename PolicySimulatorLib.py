import Agent, Team

def RandomTeams (Agents, Teams, RngGenerator, Assemble = True):
    if Assemble:
        AgentsPerTeam = int(len(Agents)/len(Teams))
        for Team in Teams:
            TeamAgents = []
            for AgentNumber in range(AgentsPerTeam):
                AgentToAssign = Agents.pop(RngGenerator.integers(0,len(Agents)))
                TeamAgents.append(AgentToAssign)  
            Team.Assemble(TeamAgents)
    else:
        for Team in Teams:
            Agents.extend(Team.Disassemble())
    return Agents

def StaticTeams (Agents, Teams, RngGenerator, Assemble = True):
    if Assemble:
        if len(Agents) > 0:
            AgentsPerTeam = int(len(Agents)/len(Teams))
            for Team in Teams:
                TeamAgents = []
                for AgentNumber in range(AgentsPerTeam):
                    AgentToAssign = Agents.pop(RngGenerator.integers(0,len(Agents)))
                    TeamAgents.append(AgentToAssign)  
                Team.Assemble(TeamAgents)
    return Agents

def NoTeams (Agents, Teams, RngGenerator, Assemble = True):
    if Assemble:
        if len(Agents) > 0:
            for Team in Teams:
                Team.Assemble([Agents.pop(RngGenerator.integers(0,len(Agents)))])
    return []

PolicyDict = {
    "Random" : RandomTeams,
    "Static" : StaticTeams,
    "Solo" : NoTeams
}