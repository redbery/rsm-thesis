# RSM Thesis

## Settings

### General
* SimulataneousSimulations - This decides how many simulations we calculate at the same time. The higher this setting the more CPU cores you will use.
* Teams - The amount of teams which will exist in the simulations.
* AgentsInTeam - The amount of agents which will be made for each team. The total amount of agents will always be Teams * AgentsInTeam.
* AgentsOptimistic - Whether the agents are optimisic about unknown arms. This will cause them to assume any arm they do not know has a mean of float_max.
* PoliciesToRun - Which policies you want to run simulations for. For each element in this list the requested amount of simulations will be ran. Each element should have a structure of [SimulatorPolicy, TeamPolicy, AgentPolicy] where the SimulatorPolicy decides how teams are formed, the TeamPolicy decides how each team reaches a decision, and the AgentPolicy decides how an agent decides what arm they would like to put forward to the rest of the team.

### Bandit
* Arms - How many arms does each bandit have.
* MeanOfMeans - What is the average mean score an arm on the bandit has.
* StdOfMeans - What is the standard deviation of the mean score of an arm on the bandit. This is used during creation of the arms to spread the mean score of the arms. Setting this to 0 would lead to every arm on the bandit having the same mean score. If MeansAreUniform this value turns from a std to a range variable where the range of the uniform distribution is between MeanOfMeans - StdOfMeans to MeanOfMeans + StdOfMeans.
* MeansAreUniform - Whether we create the arms on a uniform distribution as opposed to a normal distribution.
* MeanOfStd - What is the average standard deviation an arm has when giving scores. This setting needs to be 0 when using BinaryRewards.
* StdOfStd - What is the standard deviation of the standard deviation of the score of an arm on the bandit. This is used when creating the arm to set the std value of the arm according to a normal distribution with this and the previously mentioned value. This setting needs to be 0 when using BinaryRewards.
* BinaryRewards - When true an arm gives only a reward of 1 or 0 following a binomial distribution with probablity being the mean of the arm.

### Simulation
* Simulations - How many different simulations will we run. Each simulation uses a different generated bandit, but each simulation (with the same bandit) runs once for each policy to ensure any outliers caused due to bandit generation are visible in every policy equally.
* SimulationIterations - How many iterations we do of each simulation. The entire simulation (with the same bandit) is ran this many times. If MaintainSeedEveryIteration is enabled additional iterations do not provide much use, but when this is disabled additional iterations will even out outliers caused by randomness.
* PlaysPerSimulationIteration - How many rounds will be played in each interation. During each round all teams get 1 chance to play 1 arm.
* MaintainSeedEveryIteration - Do we reuse the randomization seed for each iteration of the simualation. Theoretically should lead to having same outcomes every iteration if set to true.
* SeedStartingNumber - What is the starting number of the seeds we will use for randomization. See the comment at the top of Main.py for how seeds are distributed, but every instance of every class should end up with a different seed, however these calculations start counting at SeedStartingNumber, so changing this will change all randomization. This number will go up every iteration if MaintainSeedEveryIteration is false.

## Policies

### Simulator (How to form teams)
* Random - Agents are randomly placed into a team at the start of every play.
* Static - Agents are randomly placed into a team at the first play and never reassigned.

### Team (How to choose an option out of the team's agents requests)
Note: Information shared is only listened to if the sample amount the sharing agent has is higher than the existing information for the agent who is listening to the information. It is never combined as we cannot know if there are duplicate information points (from when both agents were in the same team).
Note: Votes for a round of exploration are all tallied together instead of having agents vote on the specific arm they want to explore.
* Random - Randomly pick one agent in the team and use the arm they chose.
* MaxA - Listen to the agent who thinks they know the arm with the highest mean, without sharing any information.
* MaxC - Listen to the agent who thinks they know the arm with the highest mean, after everyone shares all the information they know.
* MaxO - Listen to the agent who thinks they know the arm with the highest mean, after everyone says which arms they have tried before (not what the mean was). This counteracts the effect of optimism where each agent wants to see every arm before starting to exploit.
* VoteS - Chose the arm which is requested by the most agents in a team, without sharing any information.
* VoteH - Chose the arm which is requested by the most agents in a team, after everyone shares their knowledge about the highest mean arm they have observed. Note that this will use the observed std, meaning it does not work very well when combined with agents that use estimated knowledge combinations (such as Thompson). VoteC works better for these.
* VoteC - Chose the arm which is requested by the most agents in a team, after everyone shares their knowledge about the estimated they have the highest certainty of.
* VoteO - Chose the arm which is requested by the most agents in a team, after everyone says which arms they have tried before (not what the mean was). This counteracts the effect of optimism where each agent wants to see every arm before starting to exploit.

### Agent (How to choose which arm to request)
Note: All agents in a team during a round use the same seed number for their choice of explore vs exploit (in e-greedy). This means the whole team will chose to explore at the same time if they have the same epsilon. This should prevent the real epsilon becoming a power of the agents in a team and the requested epsilon.
* Random - Choose a random arm.
* HighestMean - Choose the arm which you think has the highest mean.
* Perfect - Always choose the best arm. This uses perfect knowledge which the agent shouldn't have and is mostly useful for verification of visualizations.
* Thompson - Get a sample following Thompson sampling rules and select the arm that gave us the highest sample.
* EGreedyX - Choose following an epsilon greedy algorithm where X is the permille to explore. The valid numbers to replace X with are listed below. Making more is very easy, see PolicyAgentLib.py.
    * Valid permille settings: 200 / 20 / 10 / 5 / 4 / 3 / 2
