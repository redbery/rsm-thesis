from numpy.random import default_rng

class Arm:
    def __init__(self, Mean, Std, BinaryRewards, Seed=0) -> None:
        '''
        Initialize an arm of the bandit, which can be played to obtain a certain result/score.

        Args:
            Mean (float): The mean result playing this arm should yield.
            Std (float): The standard deviation of the result playing this arm should yield.
            BinaryRewards (bool): Indicates whether the rewards are binary (True) or continuous (False).
            Seed (int, optional): Seed used for random number generation in the arm's reward generation process. Defaults to 0.
        '''     
        self.Mean = Mean
        self.Std = Std
        self.Seed = Seed
        self.BinaryRewards = BinaryRewards
        return
    
    def InitializeRNG(self, Seed=0):
        self.Rng = default_rng(self.Seed + Seed)
    
    @property
    def Stats(self) -> dict:
        '''
        Return the statistics of this arm.

        Returns:
            dict: The statistics in the form {"Mean": float, "Std": float}
        '''        
        return {"Mean": self.Mean, "Std": self.Std}
    
    def Play(self) -> float:
        '''
        Play the arm and generate a reward.

        Returns:
            float: The reward obtained from playing the arm.
        '''
        if self.BinaryRewards:
            return float(self.Rng.binomial(1, self.Mean))
        else:
            return max(self.Rng.normal(loc=self.Mean, scale=self.Std), 0)