import Agent
from numpy.random import default_rng

class Team:
    def __init__(self, TeamNumber, Policy, Seed=0) -> None:
        self.TeamNumber = TeamNumber
        self.Agents = []
        self.Policy = Policy
        self.Rng = default_rng(Seed)
        return
    
    def Assemble(self, Agents) -> None:
        '''
        Add the passed agents to this team.

        Args:
            Agents (list): A list of agents to add to the team.
        '''        
        for Agent in Agents:
            Agent.Assign(self.TeamNumber)
            self.Agents.append(Agent)
        return

    def Disassemble(self) -> list:
        '''
        Remove all agents from the team.

        Returns:
            list: The agents which have been freed by removing them from this team.
        '''        
        FreedAgents = self.Agents
        for Agent in self.Agents:
            Agent.Assign(0)
        self.Agents = []
        return FreedAgents
    
    def PlayRound(self, Bandit, RoundNumber) -> dict:
        '''
        Play a round through the assigned policy and return the resulting score for processing.
        Most parts are done internally, such as creating memmories. The returned score is only
        for visualization.

        Args:
            Bandit (Bandit): The bandit which should be played.
            RoundNumber (int): Which round is being played currently.

        Returns:
            dict: { 'Score' : float The score which was received from the play, 'SelectedMean' : float The mean of the arm that was played, 'BestArm' : bool Was this the best arm }
        '''   
        return self.Policy(RoundNumber, self.Agents, Bandit, self.Rng)