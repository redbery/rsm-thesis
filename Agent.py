import Knowledge
import numpy, math
from numpy.random import default_rng
from sys import float_info

class Agent:
    def __init__(self, AgentNumber, Policy, Bandit, Optimistic, OptimisticSample, Seed=0, SimulatorSeed=0) -> None:      
        '''
        Initializes an agent that can be part of a team to interact with a multi-armed bandit problem, using its knowledge and policy to make decisions.

        Args:
            AgentNumber (int): The unique identifier for the agent in a team.
            Policy (func): The decision-making policy the agent will follow when selecting arms.
            Bandit (Bandit): An instance of the Bandit class representing the multi-armed bandit problem the agent will interact with.
            Optimistic (bool): Whether the agent should employ an optimistic initial value strategy.
            Seed (int, optional): Seed used for random number generation in the agent's decision-making process. Defaults to 0.
            SimulatorSeed (int, optional): Seed used for random number generation in the simulation. Defaults to 0.
        '''        
        self.TeamNumber = 0
        self.AgentNumber = AgentNumber
        self.OurKnowledge = {}
        ArmsCreated = 0
        self.Seed = Seed
        for Arm in Bandit.Arms:
            self.OurKnowledge[Arm] = Knowledge.Knowledge(Arm, Seed = self.Seed * 5 + ArmsCreated)
            ArmsCreated += 1
        self.Policy = Policy
        self.Optimistic = Optimistic
        self.OptimisticSample = OptimisticSample
        self.SimulatorSeed = SimulatorSeed
        self.Rng = default_rng(self.Seed)
        return
        
    def Assign(self, TeamNumber) -> None:
        '''
        Add the agent to a team.

        Args:
            TeamNumber (int): The number of the team the agent will be assigned to.
        '''        
        self.TeamNumber = TeamNumber
        return

    def SelectArm(self, Bandit, RoundNumber, SharedKnowledge = []) -> dict:
        '''
        Select an arm to play based on the policy.

        Args:
            Bandit (Bandit): The instance of the Bandit class representing the multi-armed bandit problem the agent will interact with.
            RoundNumber (int): The current round number in the game.
            SharedKnowledge (list, optional): A list of knowledge objects shared among agents in a team. Defaults to an empty list.

        Returns:
            dict: A dictionary containing the selected arm's information in the following format:
                {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        return self.Policy(RoundNumber, self, Bandit, self.Rng, SharedKnowledge)
    
    def RememberResult(self, Arm, Score) -> None:
        '''
        Update the agent's knowledge about a specific arm based on the received score.

        Args:
            Arm (Arm): The instance of the Arm class representing the arm for which the result is being updated.
            Score (float): The observed score from playing the arm, which will be used to update the agent's knowledge.
        '''        
        AKnowledge = self.OurKnowledge.get(Arm)
        AKnowledge.UpdateKnowledge(Score)
        return
    
    def RecallSpecificArm(self, Arm, SharedKnowledge) -> dict:
        '''
        Retrieve the agent's knowledge about a specific arm and consider shared knowledge if it has a higher sample size.

        Args:
            Arm (Arm): The instance of the Arm class representing the arm for which the information is being retrieved.
            SharedKnowledge (list): A list of knowledge objects shared among agents in a team.

        Returns:
            dict: A dictionary containing the arm's information in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        ArmDict = self.OurKnowledge[Arm].RecallObservedStatistics()
        if self.Optimistic and ArmDict["SampleSize"] == 0:
            ArmDict["MeanScore"] = float_info.max         
            
        # Check shared info for better info point about the arm
        for AKnowledge in SharedKnowledge:
            if AKnowledge["Arm"] == Arm and AKnowledge["SampleSize"] > ArmDict["SampleSize"]:
                ArmDict = AKnowledge
        
        return ArmDict
    
    def RecallBestLocallyKnownArm(self) -> dict:
        '''
        Retrieve the best locally known arm based on the highest mean score among the arms the agent has knowledge of.

        Returns:
            dict: A dictionary containing the information of the best locally known arm in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''        
        HighestMean = -1
        BestKnownArm = None
        for Arm in self.RecallAllLocallyKnownArms(MinimalSampleSize=1):
            if Arm["MeanScore"] > HighestMean:
                HighestMean = Arm["MeanScore"]
                BestKnownArm = Arm
        
        return BestKnownArm
    
    def RecallAllLocallyKnownArms(self, MinimalSampleSize = 0) -> list:
        '''
        Retrieve the agent's knowledge about all arms with a sample size greater than or equal to the specified minimal sample size.

        Args:
            MinimalSampleSize (int, optional): The minimal number of observations the agent should have about an arm to include it in the returned list. Defaults to 0.

        Returns:
            list: A list of dictionaries containing the information of all arms that meet the specified minimal sample size criterion, in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        ArmDictList = []
        for Arm in self.OurKnowledge.keys():
            ArmDict = self.OurKnowledge[Arm].RecallObservedStatistics()
            if self.Optimistic and ArmDict["SampleSize"] == 0:
                ArmDict["MeanScore"] = float_info.max 
            
            if ArmDict["SampleSize"] >= MinimalSampleSize:
                ArmDictList.append(ArmDict)     
                  
        return ArmDictList
    
    def RecallAllKnownArms(self, SharedKnowledge) -> list:
        '''
        Combine the best pieces of external knowledge with the agent's internal knowledge to create a shared knowledge bank for a single use.

        Args:
            SharedKnowledge (dict): The knowledge shared from external sources.

        Returns:
            list: The combined knowledge list containing one entry for every arm that has knowledge, selecting the entry with the most observations, in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''    
        LocallyKnownArms = self.RecallAllLocallyKnownArms()
        # Early exit if we don't have any shared knowledge
        if len(SharedKnowledge) == 0:
            return LocallyKnownArms
        
        CombinedKnowledgeDict = {}
        CombinedKnowledgeList = SharedKnowledge + LocallyKnownArms
        for Knowledge in CombinedKnowledgeList:
            # if the sample size in the combined dict for the arm in this piece of is smaller than in this piece of knowledge, replace it in the combined dict
            if CombinedKnowledgeDict.get(Knowledge["Arm"], {"SampleSize" : 0})["SampleSize"] <= Knowledge["SampleSize"]:
                CombinedKnowledgeDict[Knowledge["Arm"]] = Knowledge
        
        return list(CombinedKnowledgeDict.values())
    
    def SampleSpecificArm(self, Arm, SharedKnowledge) -> dict:
        '''
        Retrieve a sample of the agent's knowledge about a specific arm, considering shared knowledge if it has a lower standard deviation.

        Args:
            Arm (Arm): The instance of the Arm class representing the arm for which the information is being retrieved.
            SharedKnowledge (list): A list of knowledge objects shared among agents in a team.

        Returns:
            dict: A dictionary containing the arm's information in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        ArmDict = self.OurKnowledge[Arm].RecallSample()
        if self.OptimisticSample and ArmDict["SampleSize"] == 0:
            ArmDict["MeanScore"] = float_info.max         
            
        # Check shared info for better info point about the arm
        for AKnowledge in SharedKnowledge:
            if AKnowledge["Arm"] == Arm and ArmDict["StdScore"] > AKnowledge["StdScore"]:
                ArmDict = AKnowledge
        
        return ArmDict
    
    def SampleAllLocallyKnownArms(self, MinimalSampleSize = 0) -> list:
        '''
        Retrieve a sample of the agent's knowledge about all arms with a sample size greater than or equal to the specified minimal sample size.

        Args:
            MinimalSampleSize (int, optional): The minimal number of observations the agent should have about an arm to include it in the returned list. Defaults to 0.

        Returns:
            list: A list of dictionaries containing the information of all arms that meet the specified minimal sample size criterion, in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        ArmDictList = []
        for Arm in self.OurKnowledge.keys():
            ArmDict = self.OurKnowledge[Arm].RecallSample()
            if self.OptimisticSample and ArmDict["SampleSize"] == 0:
                ArmDict["MeanScore"] = float_info.max 
            
            if ArmDict["SampleSize"] >= MinimalSampleSize:
                ArmDictList.append(ArmDict)     
                  
        return ArmDictList
    
    def SampleAllKnownArms(self, SharedKnowledge) -> list:
        '''
        Combine the best pieces of external sampled knowledge with the agent's internal knowledge to create a shared knowledge bank for a single use.

        Args:
            SharedKnowledge (dict): The sampled knowledge shared from external sources.

        Returns:
            list: The combined knowledge list containing one entry for every arm that has knowledge, selecting the entry with the lowest standard deviation, in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''    
        LocallyKnownArms = self.SampleAllLocallyKnownArms()
        # Early exit if we don't have any shared knowledge
        if len(SharedKnowledge) == 0:
            return LocallyKnownArms
        
        CombinedKnowledgeDict = {}
        CombinedKnowledgeList = SharedKnowledge + LocallyKnownArms
        for Knowledge in CombinedKnowledgeList:
            # if the sample size in the combined dict for the arm in this piece of is smaller than in this piece of knowledge, replace it in the combined dict
            if CombinedKnowledgeDict.get(Knowledge["Arm"], {"StdScore" : 1000})["StdScore"] > Knowledge["StdScore"]:
                CombinedKnowledgeDict[Knowledge["Arm"]] = Knowledge
        
        return list(CombinedKnowledgeDict.values())
    
    def RecallEPSpecificArm(self, Arm, Percentile) -> dict:
        '''
        Retrieve the specified percentile information for a specific arm based on the agent's knowledge.

        Args:
            Arm (Arm): The instance of the Arm class representing the arm for which the information is being retrieved.
            Percentile (float): The desired percentile to retrieve (between 0 and 1).

        Returns:
            dict: A dictionary containing the arm's information in the following format:
                  {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''   
        ArmDict = self.OurKnowledge[Arm].RecallEstimatedPercentile(Percentile)            
        
        return ArmDict