import Arm, Agent, Bandit
from numpy.random import default_rng

def RandomArm(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select a random arm to play.

    Args:
        RoundNumber (int): The current round number.
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        RngGenerator (default_rng): Generator to use for random numbers.
        SharedKnowledge (list): List of shared knowledge elements given by other agents (defaults to []).

    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}.
    '''    
    ArmDict = {"Arm": None, "MeanScore": 0.0, "StdScore": 0.0, "SampleSize": 0}
    SelectedArmIndex = RngGenerator.integers(0,len(Bandit.Arms))
    ArmDict = Agent.RecallSpecificArm(Bandit.Arms[SelectedArmIndex], SharedKnowledge)

    return ArmDict

def PerfectArm(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select the perfect arm to play.

    Args:
        RoundNumber (int): The current round number.
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        RngGenerator (default_rng): Generator to use for random numbers.
        SharedKnowledge (list): List of shared knowledge elements given by other agents (defaults to []).

    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}.
    '''    
    ArmDict = {"Arm": None, "MeanScore": 0.0, "StdScore": 0.0, "SampleSize": 0}
    ArmDict = Agent.RecallSpecificArm(Bandit.BestArm, SharedKnowledge)

    return ArmDict
  
def HighestMeanArm(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select the arm with the highest mean known by the agent.

    Args:
        RoundNumber (int): The current round number.
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        RngGenerator (default_rng): Generator to use for random numbers.
        SharedKnowledge (list): List of shared knowledge elements given by other agents (defaults to []).

    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}.
    '''   
    PossibleArmDicts = None
    # Look through all the arm memories to find the best one
    for ArmDict in Agent.RecallAllKnownArms(SharedKnowledge):
        if not PossibleArmDicts:
            # First entry is always better than nothing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["MeanScore"] > PossibleArmDicts[0]["MeanScore"]:
            # If entry is better, replace existing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["MeanScore"] == PossibleArmDicts[0]["MeanScore"]:        
            # If entry is better, add to existing so we can grab random one         
            PossibleArmDicts.append(ArmDict)
    
    return PossibleArmDicts[RngGenerator.integers(0,len(PossibleArmDicts))]

def HighestCertaintyArm(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select the arm with the highest estimation certainty known by the agent.

    Args:
        RoundNumber (int): The current round number.
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        RngGenerator (default_rng): Generator to use for random numbers.
        SharedKnowledge (list): List of shared knowledge elements given by other agents (defaults to []).

    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}.
    '''   
    PossibleArmDicts = None
    # Look through all the arm memories to find the best one
    for ArmDict in Agent.SampleAllKnownArms(SharedKnowledge):
        if not PossibleArmDicts:
            # First entry is always better than nothing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["StdScore"] < PossibleArmDicts[0]["StdScore"]:
            # If entry is better, replace existing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["StdScore"] == PossibleArmDicts[0]["StdScore"]:        
            # If entry is better, add to existing so we can grab random one         
            PossibleArmDicts.append(ArmDict)
    
    return PossibleArmDicts[RngGenerator.integers(0,len(PossibleArmDicts))]

def ThompsonSample(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select the arm with the highest mean based on Thompson sampling.

    Args:
        RoundNumber (int): The current round number.
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        RngGenerator (default_rng): Generator to use for random numbers.
        SharedKnowledge (list): List of shared knowledge elements given by other agents (defaults to []).

    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}.
    '''   
    PossibleArmDicts = None
    # Look through all the samples to find the best one
    for ArmDict in Agent.SampleAllKnownArms(SharedKnowledge):
        if not PossibleArmDicts:
            # First entry is always better than nothing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["MeanScore"] > PossibleArmDicts[0]["MeanScore"]:
            # If entry is better, replace existing
            PossibleArmDicts = [ArmDict]
        elif ArmDict["MeanScore"] == PossibleArmDicts[0]["MeanScore"]:        
            # If entry is better, add to existing so we can grab random one         
            PossibleArmDicts.append(ArmDict)
    
    return PossibleArmDicts[RngGenerator.integers(0,len(PossibleArmDicts))]

def EpsilonGreedy(RoundNumber, Agent, Bandit, Epsilon, RngGenerator, SharedKnowledge = []) -> dict:
    '''
    Select an arm following the rules of the epsilon-greedy algorithm. Exploit/Explore choice is team wide.
    The explore call to random arm is called by the team policy to prevent someone who gets outvoted for
    explore from having their vote being useless (as the explore will never be the highest mean/voted).
    
    Args:
        RoundNumber (int): The current round number
        Agent (Agent): The agent performing the selection.
        Bandit (Bandit): The bandit we will play on.
        Epsilon (float): The epsilon to use.
        RngGenerator (default_rng): Generator to use for random numbers
        SharedKnowledge (list): List of shard knowledge elements given by other agents (defaults to [])
        
    Returns:
        dict: Arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
    '''   
    ExploitRng = default_rng(Agent.SimulatorSeed + RoundNumber + Agent.TeamNumber * 50)      
    Exploit = ExploitRng.uniform(0,1) >= Epsilon
    
    SelectedArmDict = {"Arm": None, "MeanScore": 0.0, "StdScore": 0.0, "SampleSize": 0}

    SelectedArmDict = HighestMeanArm(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge)
    SelectedArmDict["Exploit"] = Exploit
    
    return SelectedArmDict   

def Greedy(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, -1.0, RngGenerator, SharedKnowledge)  

def EpsilonGreedy1(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.001, RngGenerator, SharedKnowledge)    

def EpsilonGreedy3(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.003, RngGenerator, SharedKnowledge)

def EpsilonGreedy4(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.004, RngGenerator, SharedKnowledge)   

def EpsilonGreedy5(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.005, RngGenerator, SharedKnowledge)      

def EpsilonGreedy10(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.010, RngGenerator, SharedKnowledge)    
      
def EpsilonGreedy20(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.020, RngGenerator, SharedKnowledge)  

def EpsilonGreedy100(RoundNumber, Agent, Bandit, RngGenerator, SharedKnowledge = []) -> dict:
    return EpsilonGreedy(RoundNumber, Agent, Bandit, 0.200, RngGenerator, SharedKnowledge) 

PolicyDict = {
    "Random" : RandomArm,
    "HighestMean": HighestMeanArm,
    "Perfect" : PerfectArm,
    "Greedy" : Greedy,
    "EGreedy1" : EpsilonGreedy1,
    "EGreedy3" : EpsilonGreedy3,
    "EGreedy4" : EpsilonGreedy4,
    "EGreedy5" : EpsilonGreedy5,
    "EGreedy10" : EpsilonGreedy10,
    "EGreedy20" : EpsilonGreedy20,
    "EGreedy100" : EpsilonGreedy100,
    "Thompson" : ThompsonSample
}

