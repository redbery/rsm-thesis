import Arm, Bandit, Logger
from numpy.random import default_rng

class ManualBandit(Bandit.Bandit):
    def __init__(self, Arms, BinaryRewards = False, Seed=0) -> None:
        '''
        Initialize a multi-armed bandit with a specified number of arms, each with its own mean and standard deviation.

        Args:
            Arms (List): A list with all the arms this bandit will get.
            MeansUniform (bool, optional): If True, use a uniform distribution for the means. Note that when this is True, the standard deviation becomes a range number instead of a standard deviation. Defaults to False.
            BinaryRewards (bool, optional): If True, rewards are binary. Keep arm means between 0 and 1 when using this setting as they get clamped. Note that MeanOfStd and StdOfStd are meaningless with this setting. Defaults to False.
        '''
        self.Arms = []
        self.ArmsStats = []
        self.Seed = Seed
        self.Rng = default_rng(self.Seed)
        self.BestArm = None
        self.BinaryRewards = BinaryRewards
        
        BestArmMean = -1    
        for ArmNumber, ArmStats in enumerate(Arms):
            NewArm = Arm.Arm(ArmStats["Mean"], ArmStats["Std"], self.BinaryRewards, Seed * 12 + (ArmNumber * 2) + 1)
            self.Arms.append(NewArm)
            self.ArmsStats.append(NewArm.Stats)

            if ArmStats["Mean"] > BestArmMean:
                self.BestArm = NewArm
                BestArmMean = ArmStats["Mean"]
                
        print(Logger.LoggerPrefix() + "Created bandit with arm stats: " + str(self.ArmsStats))
        return
