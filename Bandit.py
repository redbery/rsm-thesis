import Arm, Logger
from numpy.random import default_rng

class Bandit:
    def __init__(self, Arms, MeanOfMeans, StdOfMeans, MeanOfStd, StdOfStd, MeansUniform = False, BinaryRewards = False, Seed=0) -> None:
        '''
        Initialize a multi-armed bandit with a specified number of arms, each with its own mean and standard deviation.

        Args:
            Arms (int): The number of arms this bandit will have.
            MeanOfMeans (float): The mean of the means for all arms in the bandit.
            StdOfMeans (float): The standard deviation of the means for all arms in the bandit.
            MeanOfStd (float): The mean of the standard deviations for all arms in the bandit.
            StdOfStd (float): The standard deviation of the standard deviations for all arms in the bandit.
            MeansUniform (bool, optional): If True, use a uniform distribution for the means. Note that when this is True, the standard deviation becomes a range number instead of a standard deviation. Defaults to False.
            BinaryRewards (bool, optional): If True, rewards are binary. Keep arm means between 0 and 1 when using this setting as they get clamped. Note that MeanOfStd and StdOfStd are meaningless with this setting. Defaults to False.
        '''
        self.Arms = []
        self.ArmsStats = []
        self.Seed = Seed
        self.Rng = default_rng(self.Seed)
        self.BestArm = None
        self.BinaryRewards = BinaryRewards
        self.MeansUniform = MeansUniform

        UniformArmMeans = self.Rng.uniform(MeanOfMeans-StdOfMeans, MeanOfMeans+StdOfMeans, Arms)
        
        # Warning incorrect config
        if self.BinaryRewards and (MeanOfStd != 0 or StdOfStd != 0 ):
            print("WARNING: Found binary rewards enabled while arm std is non zero. These settings do not combine. See docstring of Bandit class.")
        
        BestArmMean = -1    
        for ArmNumber in range(Arms):
            ArmMean = 0
            if self.MeansUniform:
                ArmMean = UniformArmMeans[ArmNumber]
            else:
                ArmMean = self.Rng.normal(loc=MeanOfMeans, scale=StdOfMeans)
            if self.BinaryRewards:
                ArmMean = max(0,min(1, ArmMean))
            else: 
                ArmMean = max(0, ArmMean)
            ArmStd = max(0, self.Rng.normal(loc=MeanOfStd, scale=StdOfStd))
            NewArm = Arm.Arm(ArmMean, ArmStd, self.BinaryRewards, Seed * 12 + (ArmNumber * 2) + 1)
            self.Arms.append(NewArm)
            self.ArmsStats.append(NewArm.Stats)

            if ArmMean > BestArmMean:
                self.BestArm = NewArm
                BestArmMean = ArmMean
                
        print(Logger.LoggerPrefix() + "Created bandit with arm stats: " + str(self.ArmsStats))
        return
    
    def InitializeArmRNG(self, Seed=0):
        for ArmToInitialize in self.Arms:
            ArmToInitialize.InitializeRNG(Seed)
    
    @property
    def BestArmMean(self) -> float:
        '''
        Return the mean of the best arm in this bandit.

        Returns:
            float: The mean of the best arm.
        '''        
        return self.BestArm.Mean