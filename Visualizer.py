import Logger
import uuid, os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np

mpl.use('agg')

class Visualizer:
    @staticmethod
    def VisualizeRegretCumulative (Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Regret", XAxisStart = 0) -> None:
        '''
        Visualize the portion regret per round.

        Args:
            Data (SimulationResult): The simulation result object with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Regret".
        '''          
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data.BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data.BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data.BanditArmMeanOfStd) + r", $\sigma$=" + str(Data.BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Cumulative regret " + r"$\mu$")
        
        x = np.linspace(XAxisStart, XAxisStart + len(Data.RoundRegretAverage), len(Data.RoundRegretAverage))
        YList = []
        YValues = []
        for y in Data.RoundRegretAverage:
            YList.append(y)
            YValues.append(sum(YList) / len(YList))  
        TotalY = round(sum(YList), 2)
        
        MinYAxis = min(YValues) if XAxisStart != 0 else 0
        ax.set(ylim=(MinYAxis,max(YValues)+0.01), xlim=(XAxisStart, XAxisStart + len(Data.RoundRegretAverage)))
        #ax.text((len(Data.RoundRegretAverage) / 5), max(YValues)+0.01 * 0.8, TextString)
        ax.plot(x, YValues, linewidth=2.0, label="Simulation " + str(Data.SimulatorNumber) + " (" + str(TotalY) + ")")
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
        
    @staticmethod
    def VisualizeMultiRegretCumulative (Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Multi regret", XAxisStart = 0) -> None:
        '''
        Visualize the portion regret per round for multiple data sets.

        Args:
            Data (list): The simulation result objects with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Multi regret".
        '''            
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data[0].BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data[0].BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data[0].BanditArmMeanOfStd) + r", $\sigma$=" + str(Data[0].BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Cumulative regret " + r"$\mu$")
        
        HighestY = 0.0
        LowestY = 100.0 if XAxisStart != 0 else 0
        for SimulationResult in Data:
            x = np.linspace(XAxisStart, XAxisStart + len(SimulationResult.RoundRegretAverage), len(SimulationResult.RoundRegretAverage))
            YList = []
            YValues = []
            for y in SimulationResult.RoundRegretAverage:
                YList.append(y)
                YValues.append(sum(YList) / len(YList)) 
            TotalY = round(sum(YList), 2)
            if min(YValues) < LowestY:
                 LowestY = min(YValues)
                 ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundRegretAverage)))
            if max(YValues) > HighestY:
                HighestY = max(YValues)
                ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundRegretAverage)))
            ax.plot(x, YValues, label="Simulation " + str(SimulationResult.SimulatorNumber) + " (" + str(TotalY) + ")")
        #ax.text((len(Data[0].RoundRegretAverage) / 5), HighestY+0.01 * 0.8, TextString)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
    
    @staticmethod
    def SmoothValues (Values, DatapointsToMerge):
        '''
        Smooth a list of values by merging a certain percentage of the total datapoints.

        Args:
            Values (list): The values which we want to smooth.
            DatapointsToMerge (int): How many datapoints should be merged into a single point.
        '''    
        DatapointNumber = 0
        AvgValue = 0
        AvgValues = []
        for Value in Values:
            AvgValue += Value / DatapointsToMerge
            DatapointNumber += 1
            if DatapointNumber >= DatapointsToMerge:
                AvgValues.append(AvgValue)
                AvgValue = 0
                DatapointNumber = 0      
        return AvgValues
    
    @staticmethod
    def VisualizeRegret (Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Regret", SmoothingPart=0.0, XAxisStart=0) -> None:       
        '''
        Visualize the portion regret per round.

        Args:
            Data (SimulationResult): The simulation result object with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Regret".
            SmoothingPart (float, optional): Portion of total datapoints to smooth into single point. Defaults to 0.0.
        '''          
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data.BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data.BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data.BanditArmMeanOfStd) + r", $\sigma$=" + str(Data.BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Regret " + r"$\mu$")
        
        DatapointsToMerge = int(max(SmoothingPart * len(Data.RoundRegretAverage), 1))
        YValues = Data.RoundRegretAverage.copy()
        TotalY = round(sum(YValues), 2)
        YValues = Visualizer.SmoothValues(YValues, DatapointsToMerge)
        x = np.linspace(XAxisStart, XAxisStart + len(Data.RoundRegretAverage), len(YValues))
        
        MinYAxis = min(YValues) if XAxisStart != 0 else 0
        ax.set(ylim=(MinYAxis,max(YValues)+0.01), xlim=(XAxisStart, XAxisStart + len(Data.RoundRegretAverage)))
        #ax.text((len(Data.RoundRegretAverage) / 5), max(YValues)+0.01 * 0.8, TextString)
        ax.plot(x, YValues, linewidth=2.0, label="Simulation " + str(Data.SimulatorNumber) + " (" + str(TotalY) + ")")
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
        
    @staticmethod
    def VisualizeMultiRegret (Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Multi regret", LabelOverrides = None, SmoothingPart = 0.0, XAxisStart = 0) -> None:
        '''
        Visualize the portion regret per round for multiple data sets.

        Args:
            Data (list): The simulation result objects with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Multi regret".
            LabelOverrides (list, optional): Labels we want to assign to the lines of the plot instead of the default ones. Defaults to None.
            SmoothingPart (float, optional): Portion of total datapoints to smooth into single point. Defaults to 0.0.
        '''            
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data[0].BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data[0].BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data[0].BanditArmMeanOfStd) + r", $\sigma$=" + str(Data[0].BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Regret " + r"$\mu$")
        
        HighestY = 0.0
        LowestY = 100.0 if XAxisStart != 0 else 0
        SimulationNumber = 0
        for SimulationResult in Data:
            DatapointsToMerge = int(max(SmoothingPart * len(SimulationResult.RoundRegretAverage), 1))       
            YValues = SimulationResult.RoundRegretAverage.copy()
            TotalY = round(sum(YValues), 2)
            YValues = Visualizer.SmoothValues(YValues, DatapointsToMerge)
            x = np.linspace(XAxisStart, XAxisStart + len(SimulationResult.RoundRegretAverage), len(YValues))
            if min(YValues) < LowestY:
                 LowestY = min(YValues)
            if max(YValues) > HighestY:
                HighestY = max(YValues)
            ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundRegretAverage)))
            
            if LabelOverrides:
                ax.plot(x, YValues, label=LabelOverrides[SimulationNumber] + " (" + str(TotalY) + ")")
            else:
                ax.plot(x, YValues, label="Simulation " + str(SimulationResult.SimulatorNumber) + " (" + str(TotalY) + ")")
            SimulationNumber += 1
        
        #ax.text((len(Data[0].RoundRegretAverage) / 5), HighestY+0.01 * 0.8, TextString)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
    
    @staticmethod
    def VisualizeOptimalPlaysCumulative(Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Optimal plays", XAxisStart = 0) -> None:
        '''
        Visualize the portion optimal plays per round.

        Args:
            Data (SimulationResult): The simulation result object with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Optimal plays".
        '''          
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data.BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data.BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data.BanditArmMeanOfStd) + r", $\sigma$=" + str(Data.BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Cumulative portion optimal")
        
        x = np.linspace(XAxisStart, XAxisStart + len(Data.RoundPortionBest), len(Data.RoundPortionBest))
        YList = []
        YValues = []
        for y in Data.RoundPortionBest:
            YList.append(y)
            YValues.append(sum(YList) / (len(YList)))  
        
        MinYAxis = min(YValues) if XAxisStart != 0 else 0
        ax.set(ylim=(MinYAxis,max(YValues)+0.01), xlim=(XAxisStart, XAxisStart + len(Data.RoundPortionBest)))
        #ax.text((len(Data.RoundPortionBest) / 5), max(YValues)+0.01 * 0.8, TextString)
        ax.plot(x, YValues, linewidth=2.0)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
        
    @staticmethod
    def VisualizeMultiOptimalPlaysCumulative(Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Multi optimal plays", XAxisStart = 0) -> None:
        '''
        Visualize the portion optimal plays per round for multiple data sets.

        Args:
            Data (list): The simulation result objects with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Multi regret".
        '''            
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data[0].BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data[0].BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data[0].BanditArmMeanOfStd) + r", $\sigma$=" + str(Data[0].BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Cumulative portion optimal")
        
        HighestY = 0.0
        LowestY = 100.0 if XAxisStart != 0 else 0
        for SimulationResult in Data:
            x = np.linspace(XAxisStart, XAxisStart + len(SimulationResult.RoundPortionBest), len(SimulationResult.RoundPortionBest))
            YList = []
            YValues = []
            for y in SimulationResult.RoundPortionBest:
                YList.append(y)
                YValues.append(sum(YList) / (len(YList)))  
                
            if min(YValues) < LowestY:
                 LowestY = min(YValues)
                 ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundPortionBest)))
            if max(YValues) > HighestY:
                HighestY = max(YValues)
                ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundPortionBest)))
                
            ax.plot(x, YValues, label="Simulation " + str(SimulationResult.SimulatorNumber))
        #ax.text((len(Data[0].RoundPortionBest) / 5), HighestY+0.01 * 0.8, TextString)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
        
    @staticmethod
    def VisualizeOptimalPlays(Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Optimal plays", SmoothingPart = 0.0, XAxisStart = 0) -> None:
        '''
        Visualize the portion optimal plays per round.

        Args:
            Data (SimulationResult): The simulation result object with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Optimal plays".
            SmoothingPart (float, optional): Portion of total datapoints to smooth into single point. Defaults to 0.0.
        '''          
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data.BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data.BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data.BanditArmMeanOfStd) + r", $\sigma$=" + str(Data.BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Portion optimal")
        
        DatapointsToMerge = int(max(SmoothingPart * len(Data.RoundRegretAverage), 1))
        YValues = Data.RoundPortionBest.copy()
        YValues = Visualizer.SmoothValues(YValues, DatapointsToMerge)      
        x = np.linspace(XAxisStart, XAxisStart + len(Data.RoundPortionBest), len(YValues)) 
        MinYAxis = min(YValues) if XAxisStart != 0 else 0
        ax.set(ylim=(MinYAxis,max(YValues)+0.01), xlim=(XAxisStart, XAxisStart + len(Data.RoundPortionBest)))
        #ax.text((len(Data.RoundPortionBest) / 5), max(YValues)+0.01 * 0.8, TextString)
        ax.plot(x, YValues, linewidth=2.0)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()
        
    @staticmethod
    def VisualizeMultiOptimalPlays(Data, SubFolder = str(uuid.uuid1()), FileName = str(uuid.uuid1()), PlotTitle = "Multi optimal plays", LabelOverrides = None, SmoothingPart =0.0, XAxisStart = 0) -> None:
        '''
        Visualize the portion optimal plays per round for multiple data sets.

        Args:
            Data (list): The simulation result objects with all the data we want to visualize
            SubFolder (str, optional): Name of the subfolder we want the plot stored. Defaults to str(uuid.uuid1()).
            FileName (str, optional): Name we want the plot stored as. Defaults to str(uuid.uuid1()).
            PlotTitle (str, optional): Name we want to show on the plot. Defaults to "Multi regret".
            LabelOverrides (list, optional): Labels we want to assign to the lines of the plot instead of the default ones. Defaults to None.
            SmoothingPart (float, optional): Portion of total datapoints to smooth into single point. Defaults to 0.0.
        '''            
        fig, ax = plt.subplots(figsize=(16, 8)) 
        ax.grid(visible=True, which="both", axis="both")
        TextString =  "Bandit arm mean: " r"$\mu$=" + str(Data[0].BanditArmMeanOfMeans) + r", $\sigma$=" + str(Data[0].BanditArmStdOfMeans) + "\nBandit arm std: " + r"$\mu$=" + str(Data[0].BanditArmMeanOfStd) + r", $\sigma$=" + str(Data[0].BanditArmStdOfStd)
        plt.title(PlotTitle)
        plt.xlabel("Round")
        plt.ylabel("Portion optimal")
        
        HighestY = 0.0
        LowestY = 100.0 if XAxisStart != 0 else 0
        SimulationNumber = 0
        for SimulationResult in Data:
            DatapointsToMerge = int(max(SmoothingPart * len(SimulationResult.RoundRegretAverage), 1))
            YValues = SimulationResult.RoundPortionBest.copy()
            YValues = Visualizer.SmoothValues(YValues, DatapointsToMerge) 
            x = np.linspace(XAxisStart, XAxisStart + len(SimulationResult.RoundPortionBest), len(YValues))
            if min(YValues) < LowestY:
                LowestY = min(YValues)
            if max(YValues) > HighestY:
                HighestY = max(YValues)
            ax.set(ylim=(LowestY,HighestY+0.01), xlim=(XAxisStart, XAxisStart + len(SimulationResult.RoundPortionBest)))
                        
            if LabelOverrides:
                ax.plot(x, YValues, label=LabelOverrides[SimulationNumber])
            else:
                ax.plot(x, YValues, label="Simulation " + str(SimulationResult.SimulatorNumber))
            SimulationNumber += 1
        
        #ax.text((len(Data[0].RoundPortionBest) / 5), HighestY+0.01 * 0.8, TextString)
        ax.legend(loc="center right", bbox_to_anchor=(0.95,0.5))

        if not os.path.exists("./Visualizer/" + SubFolder):
            os.makedirs("./Visualizer/" + SubFolder)
        ImagePath = "./Visualizer/" + SubFolder + "/" + FileName + ".png"
        
        plt.tight_layout
        plt.savefig(ImagePath)
        print(Logger.LoggerPrefix() + "Created plot at: " + ImagePath)
        plt.close("all")
        plt.clf()
        plt.cla()