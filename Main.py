import Bandit, Logger, ManualBandit, Simulator, Visualizer
import json, multiprocessing, math
from datetime import datetime

# Random generators exist in Simulators, Bandits, Knowledge, Arms, Teams, and Agents. Policies use the generators passed by their parent.
# The generators use seed numbers of SimulatorSeedNumber * ClassModifier + InstanceNumber. Where simulator seed number is SeedStartingNumber * ClassModifier + InstanceNumber (+ IterationNumber unless MaintainSeedEveryIteration)
# Exception is Bandit which does not add SimulatorSeedNumber but instead adds SeedStartingNumber and Knowledge which instead adds Agent seed.
# The Class modifier is 10, 20, 5, 12 (+1), 30, 12. This is done to attempt to have less overlap in seeds while maintaining same seeds in every simulation.
# This should make sure every instance of every class has a different seed in different simulations (and iterations depending on the setting), but restarting the entire program will lead to the exact same results

def CreateBandits(Settings):
    BanditList = []
    if len(Settings["BanditOverride"]) > 0:
        for BanditNumber, BanditOverride in enumerate(Settings["BanditOverride"]):
            BanditList.append(ManualBandit.ManualBandit(BanditOverride, 
                               BinaryRewards = Settings["Bandit"]["BinaryRewards"], Seed = BanditNumber*20))     
    else:    
        for BanditNumber in range(Settings["Simulation"]["Simulations"]):
            BanditList.append(Bandit.Bandit(Settings["Bandit"]["Arms"], Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], 
                               Settings["Bandit"]["MeanOfStd"], Settings["Bandit"]["StdOfStd"], MeansUniform=Settings["Bandit"]["MeansAreUniform"], 
                               BinaryRewards = Settings["Bandit"]["BinaryRewards"], Seed = BanditNumber*20))
    return BanditList

def CreateSimulators(Settings, Policy, BanditList):
    Simulators = []
    for SimulatorNumber in range(len(BanditList)):
        for SimulatorIterationNumber in range(Settings["Simulation"]["SimulationIterations"]):
            SeedNumber = Settings["Simulation"]["SeedStartingNumber"] + (SimulatorNumber * 10)
            SeedNumber = SeedNumber if Settings["Simulation"]["MaintainSeedEveryIteration"] else SeedNumber + SimulatorIterationNumber
            Simulators.append(Simulator.Simulator(SimulatorNumber + 1, BanditList[SimulatorNumber], Teams=Settings["Teams"],
                                                  AgentsPerTeam=Settings["AgentsInTeam"], AgentsOptimistic = Settings["AgentsOptimistic"], 
                                                  AgentsOptimisticSample = Settings["AgentsOptimisticSample"], Policy=Policy, Seed = SeedNumber))
    return Simulators

def RunSimulators(Settings, Simulators, CurrentPolicyIndex, CacheToCopy = None):
    NumberOfPolicies = len(Settings["PoliciesToRun"])
    ProcessManager = multiprocessing.Manager()
    SimulatorReturnDict = ProcessManager.dict()
    WorkerNumber = 0
    RequiredCycles = math.ceil(len(Simulators) / Settings["SimulataneousSimulations"])
    for Cycle in range(RequiredCycles):
        Jobs = []
        SubsetStart = Cycle * Settings["SimulataneousSimulations"]
        SubsetEnd = (Cycle+1) * Settings["SimulataneousSimulations"]
        SimulatorsSubset = Simulators[SubsetStart:SubsetEnd]
        for ASimulator in SimulatorsSubset:
            NewJob = multiprocessing.Process(target=ASimulator.Run, kwargs={"WorkerNumber":WorkerNumber, "ReturnDict":SimulatorReturnDict, "Plays":Settings["Simulation"]["PlaysPerSimulationIteration"], "CacheToCopy":CacheToCopy})
            Jobs.append(NewJob)
            NewJob.start()
            WorkerNumber += 1
        for Worker in Jobs:
            Worker.join()
        print(Logger.LoggerPrefix() + "Finished simulation " + str(CurrentPolicyIndex * len(Simulators) + SubsetStart + 1) + " to " + str(CurrentPolicyIndex * len(Simulators) + WorkerNumber) + " out of " + str(NumberOfPolicies * len(Simulators)))
        
    return SimulatorReturnDict

def ProcessVisualizeSinglePolicyBaseResults(SimulationResultLists, Settings, ResultSubFolder, Policy):
    # Turn list of lists into a single list
    SimulationResults = [item for sublist in SimulationResultLists for item in sublist]
    
    # Make an average simulation result which combines all simulation results
    SimulationResultsAverage = Simulator.SimulationResult.CreateAverageSimulationResult(SimulationResults, Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], Settings["Bandit"]["MeanOfStd"], Settings["Bandit"]["StdOfStd"],)
    Visualizer.Visualizer.VisualizeRegret(SimulationResultsAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "Avg", PlotTitle = Policy[2] + " 1% smoothed regret average", SmoothingPart = 0.01)
    Visualizer.Visualizer.VisualizeOptimalPlays(SimulationResultsAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "Avg", PlotTitle = Policy[2] + " 1% smoothed optimal plays average", SmoothingPart = 0.01)
    Visualizer.Visualizer.VisualizeRegret(SimulationResultsAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "AvgRaw", PlotTitle = Policy[2] + " regret average")
    Visualizer.Visualizer.VisualizeOptimalPlays(SimulationResultsAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "AvgRaw", PlotTitle = Policy[2] + " optimal plays average")
    
    # Make individual graphs for each simulation with all iterations in it  
    SimulationResultsIterationsAverages = []
    for SimulationResultList in SimulationResultLists: 
        Visualizer.Visualizer.VisualizeMultiRegret(SimulationResultList, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + str(SimulationResultList[0].SimulatorNumber),
                                                   PlotTitle = Policy[2] + " 2.5% smoothed regret simulation", SmoothingPart = 0.025)
        Visualizer.Visualizer.VisualizeMultiOptimalPlays(SimulationResultList, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + str(SimulationResultList[0].SimulatorNumber),
                                                         PlotTitle = Policy[2] + " 2.5% smoothed optimal plays simulation", SmoothingPart = 0.025)
        SimulationResultsIterationsAverages.append(Simulator.SimulationResult.CreateAverageSimulationResult(SimulationResultList, Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], Settings["Bandit"]["MeanOfStd"], 
                                                                                                            Settings["Bandit"]["StdOfStd"], BanditArmStats = BanditList[(SimulationResultList[0].SimulatorNumber - 1)].ArmsStats, 
                                                                                                            SimulatorNumber = SimulationResultList[0].SimulatorNumber))  
    # Visualize an average simulation result which combines all simulation iteration results
    Visualizer.Visualizer.VisualizeMultiRegret(SimulationResultsIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "Multi", 
                                               PlotTitle = Policy[2] + " 0.5% smoothed regret multiple simulations", SmoothingPart = 0.005)
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(SimulationResultsIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "Multi", 
                                                     PlotTitle = Policy[2] + " 0.5% smoothed optimal plays multiple simulations", SmoothingPart = 0.005)
    Visualizer.Visualizer.VisualizeMultiRegret(SimulationResultsIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "MultiRaw", 
                                               PlotTitle = Policy[2] + " regret multiple simulations")
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(SimulationResultsIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "MultiRaw", 
                                                     PlotTitle = Policy[2] + " optimal plays multiple simulations")
    return SimulationResultsAverage
    

def ProcessVisualizeSinglePolicyZoomedResults(SimulationResultZoomedLists, Settings, ResultSubFolder, Policy):
    # Turn list of lists into a single list
    SimulationResultsZoomed = [item for sublist in SimulationResultZoomedLists for item in sublist]
    
    # Make an average simulation result which combines all simulation results
    SimulationResultsZoomedAverage = Simulator.SimulationResult.CreateAverageSimulationResult(SimulationResultsZoomed, Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], Settings["Bandit"]["MeanOfStd"], Settings["Bandit"]["StdOfStd"],)
    Visualizer.Visualizer.VisualizeRegret(SimulationResultsZoomedAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "AvgZoomed", PlotTitle = Policy[2] + " 2% smoothed regret average", SmoothingPart = 0.02, XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeOptimalPlays(SimulationResultsZoomedAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "AvgZoomed", PlotTitle = Policy[2] + " 2% smoothed optimal plays average", SmoothingPart = 0.02, XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeRegret(SimulationResultsZoomedAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "AvgZoomedRaw", PlotTitle = Policy[2] + " regret average", XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeOptimalPlays(SimulationResultsZoomedAverage, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "AvgZoomedRaw", PlotTitle = Policy[2] + " optimal plays average", XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    
    # Make individual graphs for each simulation with all iterations in it  
    SimulationResultsZoomedIterationsAverages = []
    for SimulationResultZoomedList in SimulationResultZoomedLists: 
        SimulationResultsZoomedIterationsAverages.append(Simulator.SimulationResult.CreateAverageSimulationResult(SimulationResultZoomedList, Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], Settings["Bandit"]["MeanOfStd"], 
                                                                                                            Settings["Bandit"]["StdOfStd"], BanditArmStats = BanditList[(SimulationResultZoomedList[0].SimulatorNumber - 1)].ArmsStats, 
                                                                                                            SimulatorNumber = SimulationResultZoomedList[0].SimulatorNumber))  
    
    # Visualize an average simulation result which combines all simulation iteration results
    Visualizer.Visualizer.VisualizeMultiRegret(SimulationResultsZoomedIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "MultiZoomed", 
                                               PlotTitle = Policy[2] + " 1% smoothed regret multiple simulations", SmoothingPart = 0.01, XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(SimulationResultsZoomedIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "MultiZoomed", 
                                                     PlotTitle = Policy[2] + " 1% smoothed optimal plays multiple simulations", SmoothingPart = 0.01, XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeMultiRegret(SimulationResultsZoomedIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Regret" + "MultiZoomedRaw", 
                                               PlotTitle = Policy[2] + " regret multiple simulations", XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(SimulationResultsZoomedIterationsAverages, SubFolder = ResultSubFolder, FileName = Policy[2] + Policy[1] + Policy[0] + "Optimal" + "MultiZoomedRaw", 
                                                     PlotTitle = Policy[2] + " optimal plays multiple simulations", XAxisStart = int(9 * len(SimulationResultsZoomed[0].RoundRegretAverage)))  
    return SimulationResultsZoomedAverage
    
def ProcessVisualizeMultiPolicyResults(AllPolicyAveragesResults, AllPolicyZoomedAveragesResults, ResultSubFolder, AllPolicyAveragesResultsLabels):
    Visualizer.Visualizer.VisualizeMultiRegret(AllPolicyAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyRegretMulti", PlotTitle = "All policies 0.5% smoothed regret", LabelOverrides=AllPolicyAveragesResultsLabels, SmoothingPart = 0.005)
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(AllPolicyAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyOptimalMulti", PlotTitle = "All policies 0.5% smoothed optimal plays", LabelOverrides=AllPolicyAveragesResultsLabels, SmoothingPart = 0.005)
    Visualizer.Visualizer.VisualizeMultiRegret(AllPolicyAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyRegretMultiRaw", PlotTitle = "All policies regret", LabelOverrides=AllPolicyAveragesResultsLabels)
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(AllPolicyAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyOptimalMultiRaw", PlotTitle = "All policies optimal plays", LabelOverrides=AllPolicyAveragesResultsLabels)
    Visualizer.Visualizer.VisualizeMultiRegret(AllPolicyZoomedAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyRegretMultiZoomed", PlotTitle = "All policies 4% smoothed regret", LabelOverrides=AllPolicyAveragesResultsLabels, SmoothingPart = 0.04, XAxisStart = int(0.9 * len(AllPolicyAveragesResults[0].RoundRegretAverage)) )
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(AllPolicyZoomedAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyOptimalMultiZoomed", PlotTitle = "All policies 4% smoothed optimal plays", LabelOverrides=AllPolicyAveragesResultsLabels, SmoothingPart = 0.04, XAxisStart = int(0.9 * len(AllPolicyAveragesResults[0].RoundRegretAverage)))
    Visualizer.Visualizer.VisualizeMultiRegret(AllPolicyZoomedAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyRegretMultiZoomedRaw", PlotTitle = "All policies regret", LabelOverrides=AllPolicyAveragesResultsLabels, XAxisStart = int(0.9 * len(AllPolicyAveragesResults[0].RoundRegretAverage)) )
    Visualizer.Visualizer.VisualizeMultiOptimalPlays(AllPolicyZoomedAveragesResults, SubFolder = ResultSubFolder, FileName = "AllPolicyOptimalMultiZoomedRaw", PlotTitle = "All policies optimal plays", LabelOverrides=AllPolicyAveragesResultsLabels, XAxisStart = int(0.9 * len(AllPolicyAveragesResults[0].RoundRegretAverage)))
    


if __name__ == "__main__":
    ActiveLogger = Logger.Logger()
    print(Logger.LoggerPrefix() + " Log for: Main")
    
    Settings = {}
    with open("Settings.json", "r") as f:
        Settings = json.load(f)
    
    PPFCache = {}
    with open('CachedBetaPPF.json', 'r') as f:
        PPFCache = json.load(f)
    
    ResultSubFolder = str(datetime.now()).replace(":", "_").replace(" ", "_")
        
    BanditList = CreateBandits(Settings)
    
    AllPolicyAveragesResults = []
    AllPolicyZoomedAveragesResults = []
    AllPolicyAveragesResultsLabels = []
    
    CurrentPolicyIndex = -1
    for Policy in Settings["PoliciesToRun"]:
        CurrentPolicyIndex += 1
        
        AllPolicyAveragesResultsLabels.append(Policy[2] + Policy[1] + Policy[0])
        
        Simulators = CreateSimulators(Settings, Policy, BanditList)
        SimulatorReturnDict = RunSimulators(Settings, Simulators, CurrentPolicyIndex, CacheToCopy = PPFCache)
        
        # Prepare output variables
        SimulationResultLists = []
        SimulationResultZoomedLists = []
        for SimulatorNumber in range(len(BanditList)):
            SimulationResultLists.append([])
            SimulationResultZoomedLists.append([])
        
        # Process task results as they are available and make simulation results out of them
        for ReturnValue in SimulatorReturnDict.values():
            SimulationResult = Simulator.SimulationResult(ReturnValue["RoundRegretAverage"], ReturnValue["TotalRegret"], ReturnValue["RoundPortionBest"], ReturnValue["TotalPortionBest"], 
                                                          ReturnValue["BestArmScore"], Settings["Bandit"]["MeanOfMeans"], Settings["Bandit"]["StdOfMeans"], Settings["Bandit"]["MeanOfStd"], Settings["Bandit"]["StdOfStd"], 
                                                          BanditArmStats = BanditList[(ReturnValue["SimulatorNumber"]-1)].ArmsStats, SimulatorNumber = ReturnValue["SimulatorNumber"])
            SimulationResultLists[(SimulationResult.SimulatorNumber-1)].append(SimulationResult)
            # Make a 10% zoomed version of the simulation result
            SimulationResultZoomed = Simulator.SimulationResult.ZoomSimulationResult(SimulationResult, 0.9, 1.0)
            SimulationResultZoomedLists[(SimulationResultZoomed.SimulatorNumber-1)].append(SimulationResultZoomed)
        
        AllPolicyAveragesResults.append(ProcessVisualizeSinglePolicyBaseResults(SimulationResultLists, Settings, ResultSubFolder, Policy))
        AllPolicyZoomedAveragesResults.append(ProcessVisualizeSinglePolicyZoomedResults(SimulationResultZoomedLists, Settings, ResultSubFolder, Policy))
        
        # Clean before next policy
        for ASimulator in Simulators:
            del ASimulator
    
    ProcessVisualizeMultiPolicyResults(AllPolicyAveragesResults, AllPolicyZoomedAveragesResults, ResultSubFolder, AllPolicyAveragesResultsLabels)
        
