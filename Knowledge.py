import HelperFunctionLib
import numpy, scipy
from numpy.random import default_rng

class Knowledge:
    def __init__(self, Arm, Seed = 0) -> None:
        '''
        Initialize an instance of knowledge for a single arm. This instance contains
        the current knowledge about the arm and is used when asking an agent which
        arm they would like to play (within a policy).

        Args:
            Arm (Arm): The arm for which this knowledge instance is being created.
            Seed (int, optional): Seed for random number generation. Defaults to 0.
        '''
        self.Arm = Arm
        self.SampleSize = 0
        self.ObservedMean = 0.0
        self.ObservedSummedVariance = 0.0
        self.PriorStd = 100 # This is a random guess std that decides how likely under explored arms are to get tried.
        self.CachedSample = None
        self.Rng = default_rng(Seed)
        return
        
    def UpdateKnowledge(self, Score) -> None:
        '''
        Update the knowledge instance with a new score obtained from the arm.

        Args:
            Score (float): The score obtained from playing the arm.
        '''
        self.SampleSize += 1
        NewObservedMean = self.ObservedMean + (Score - self.ObservedMean) / self.SampleSize
        self.ObservedSummedVariance += (Score - self.ObservedMean) * (Score - NewObservedMean)
        self.ObservedMean = NewObservedMean 
        return
    
    def CalculateNormalStandardDeviation(self) -> float:
        '''
        Calculate the standard deviation of the observed scores from the arm.

        Returns:
            float: The calculated standard deviation, or the prior standard deviation if sample size is not greater than 1.
        '''
        if self.SampleSize > 1:
            return numpy.sqrt(self.ObservedSummedVariance / (self.SampleSize - 1))
        else:
            return self.PriorStd
    
    def CalculateBetaStandardDeviation(self) -> float:
        '''
        Calculate the standard deviation of the observed scores from the arm, assuming a beta distribution.

        Returns:
            float: The calculated standard deviation based on successes and failures.
        '''
        Successes = self.ObservedMean * self.SampleSize
        Failures = self.SampleSize - Successes
        return numpy.sqrt(max(Successes*Failures, 1)/max((Successes+Failures)**2*(Successes+Failures+1), 1))
       
    def RecallObservedStatistics(self) -> dict:
        '''
        Retrieve the observed statistics for an arm.

        Returns:
            dict: A dictionary containing arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''
        return {'Arm': self.Arm, 'MeanScore': self.ObservedMean, 'StdScore': self.CalculateNormalStandardDeviation(), 'SampleSize': self.SampleSize}
     
    def RecallObservedPercentile(self, Percentile) -> dict:
        '''
         Retrieve the observed percentile for an arm.

        Args:
            Percentile (float): The percentile to calculate for the observed scores.

        Returns:
            dict: A dictionary containing arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''
        MeanAtPercentile = scipy.stats.norm.ppf(Percentile, self.ObservedMean, self.CalculateNormalStandardDeviation())
        return {'Arm': self.Arm, 'MeanScore': MeanAtPercentile, 'StdScore': self.CalculateNormalStandardDeviation(), 'SampleSize': self.SampleSize}
    
    def RecallEstimatedPercentile(self, Percentile) -> dict:
        '''
        Retrieve the estimated percentile for an arm.

        Args:
            Percentile (float): The percentile to calculate for the estimated scores.

        Returns:
            dict: A dictionary containing arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''
        MeanAtPercentile = 0
        EstimatedMeanEstimationStd = 0
        
        if self.Arm.BinaryRewards:
            Successes = int(self.ObservedMean * self.SampleSize)
            Failures = self.SampleSize - Successes
            MeanAtPercentile = HelperFunctionLib.CachedBetaPPF(Percentile, Successes, Failures)
            EstimatedMeanEstimationStd = self.CalculateBetaStandardDeviation()
        else:
            CalculatedStd = self.CalculateNormalStandardDeviation()
            # Prevent zero divisors
            if CalculatedStd == 0:
                CalculatedStd = numpy.sqrt(0.00000001)     
            EstimatedMeanEstimationStd = numpy.sqrt((1 / self.PriorStd**2 + self.SampleSize / CalculatedStd**2)**-1)
            EstimatedMean = (EstimatedMeanEstimationStd**2) * (self.ObservedMean * self.SampleSize / CalculatedStd**2)
            MeanAtPercentile = scipy.stats.norm.ppf(Percentile, EstimatedMean, EstimatedMeanEstimationStd)
        
        return {'Arm': self.Arm, 'MeanScore': MeanAtPercentile, 'StdScore': EstimatedMeanEstimationStd, 'SampleSize': self.SampleSize}
    
    def RecallSample(self) -> dict:  
        '''
        Retrieve a single random sample from the current knowledge of an arm.

        Returns:
            dict: A dictionary containing arm information in the form {'Arm': Arm, 'MeanScore': float, 'StdScore': float, 'SampleSize': int}
        '''
        MeanSampled = 0
        EstimatedMeanEstimationStd = 0
        
        if self.Arm.BinaryRewards:
            Successes = self.ObservedMean * self.SampleSize
            Failures = self.SampleSize - Successes
            MeanSampled = self.Rng.beta(a=Successes+1, b=Failures+1)
            EstimatedMeanEstimationStd = self.CalculateBetaStandardDeviation()
        else:
            CalculatedStd = self.CalculateNormalStandardDeviation()
            # Prevent zero divisors
            if CalculatedStd == 0:
                CalculatedStd = numpy.sqrt(0.00000001)        
            EstimatedMeanEstimationStd = numpy.sqrt((1 / self.PriorStd**2 + self.SampleSize / CalculatedStd**2)**-1)
            EstimatedMean = (EstimatedMeanEstimationStd**2) * (self.ObservedMean * self.SampleSize / CalculatedStd**2)
            MeanSampled = self.Rng.normal(loc=EstimatedMean, scale=EstimatedMeanEstimationStd)
            
        self.CachedSample = {'Arm': self.Arm, 'MeanScore': MeanSampled, 'StdScore': EstimatedMeanEstimationStd, 'SampleSize': self.SampleSize}

        return self.CachedSample
    
    