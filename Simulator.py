import Agent, HelperFunctionLib, Logger, PolicyAgentLib, PolicySimulatorLib, PolicyTeamLib, Team, Visualizer
import numpy
from numpy.random import default_rng

class Simulator:
    def __init__(self, SimulatorNumber, Bandit, Teams=1, AgentsPerTeam=1, AgentsOptimistic = False, AgentsOptimisticSample = False, Policy=[PolicySimulatorLib.PolicyDict["Random"], PolicyTeamLib.PolicyDict["Random"], PolicyAgentLib.PolicyDict["Random"]], Seed=0) -> None:
        '''
        The simulator is the main object that handles creation and administration of all the other
        objects. It will have a certain result at the end of the simulation which is what will be
        studied to decide if the policy was good.

        Args:
            SimulatorNumber (int): Number simulator this belongs to.
            Bandit (Bandit): The bandit we use.
            Teams (int, optional): The amount of teams in the simulation. Defaults to 1.
            AgentsPerTeam (int, optional): The amount of agents in a team. Defaults to 1.
            Policy (func, optional): The policies to run in form [TeamCreation, TeamAgentAnswerSelection, AgentAnswerSelection]
        '''        
        self.MarkedForDestruction = False
        self.SimulatorNumber = SimulatorNumber
        self.Results = {"Ran" : False, "SimulatorNumber" : self.SimulatorNumber, "RoundRegretAverage": [], "RoundPortionBest" : [], "TotalRegret": []}
        self.Bandit = Bandit
        self.Teams = []
        self.Policy = Policy
        self.Rng = default_rng(Seed)
        Bandit.InitializeArmRNG(Seed)
        for TeamNumber in range(Teams):
            self.Teams.append(Team.Team(TeamNumber + 1, PolicyTeamLib.PolicyDict[self.Policy[1]], Seed = Seed * 5 + (TeamNumber)))
        self.AgentsPerTeam = AgentsPerTeam
        self.AvailableAgents = []
        for AgentNumber in range(Teams * self.AgentsPerTeam):
            self.AvailableAgents.append(Agent.Agent(AgentNumber + 1, PolicyAgentLib.PolicyDict[self.Policy[2]], self.Bandit, AgentsOptimistic, AgentsOptimisticSample, Seed = Seed * 12 + (AgentNumber * 2) + 1, SimulatorSeed = Seed))
            
        return  
              
    def Run(self, WorkerNumber = 0, ReturnDict = [], Plays=100, CacheToCopy=None) -> dict:
        '''
        Runs the simulation with the settings passed during creation of the object. The results are saved
        in the object and retrieved/handled from the outside.

        Args:
            Plays (int, optional): The amount of plays we will do in this simulation. Defaults to 100.

        Returns:
            dict: The results of the simulation.
        '''     
        # Instanciate custom logger. Needed here as well due to multi threading.
        ActiveLogger = Logger.Logger()  
        print(Logger.LoggerPrefix() + "Log for: " + str(self.Policy))
        
        # Instanciate cache
        HelperFunctionLib.CachedBetaPPF(0.9, 0, 0, Cache = CacheToCopy)
         
        if(self.MarkedForDestruction):
            return
        RoundNumber = 0
        for Play in range(Plays):
            # Assemble the teams
            PolicySimulatorLib.PolicyDict[self.Policy[0]](self.AvailableAgents, self.Teams, self.Rng, Assemble=True)
            
            # Play a round
            TotalScore = 0
            PortionBest = 0.0
            for Team in self.Teams:
                TeamResult = Team.PlayRound(self.Bandit, RoundNumber)
                TotalScore += TeamResult["SelectedMean"]
                PortionBest += TeamResult["BestArm"] / len(self.Teams)
            RoundRegret = self.Bandit.BestArmMean - (TotalScore/len(self.Teams))
            self.Results["RoundRegretAverage"].append(RoundRegret)
            self.Results["RoundPortionBest"].append(PortionBest)
            RoundNumber += 1
            
            # Disassemble the teams
            PolicySimulatorLib.PolicyDict[self.Policy[0]](self.AvailableAgents, self.Teams, self.Rng, Assemble=False)
            
        self.Results["Ran"] = True
        
        # Calculate results
        self.Results["BestArmScore"] = self.Bandit.BestArmMean
        self.Results["TotalRegret"] = sum(self.Results["RoundRegretAverage"])
        self.Results["TotalPortionBest"] = sum(self.Results["RoundPortionBest"]) / len(self.Results["RoundPortionBest"])
        
        # Break down
        self.MarkedForDestruction = True
        for Team in self.Teams:
            del Team
        for Agent in self.AvailableAgents:
            del Agent
            
        print(Logger.LoggerPrefix() + " Total regret: " + str(round(sum(self.Results["RoundRegretAverage"]), 2)))
        
        ReturnDict[WorkerNumber] = self.Results
        return self.Results
    
class SimulationResult:
    def __init__(self, RoundRegretAverage, TotalRegret, RoundPortionBest, TotalPortionBest, BestArmScore, BanditArmMeanOfMeans, BanditArmStdOfMeans, BanditArmMeanOfStd, BanditArmStdOfStd, BanditArmStats = {}, SimulatorNumber = 1) -> None:
        self.RoundRegretAverage = RoundRegretAverage
        self.TotalRegret = TotalRegret
        self.RoundPortionBest = RoundPortionBest
        self.TotalPortionBest = TotalPortionBest
        self.BestArmScore = BestArmScore
        self.BanditArmMeanOfMeans = BanditArmMeanOfMeans
        self.BanditArmStdOfMeans = BanditArmStdOfMeans
        self.BanditArmMeanOfStd = BanditArmMeanOfStd
        self.BanditArmStdOfStd = BanditArmStdOfStd
        self.BanditArmStats = BanditArmStats
        self.SimulatorNumber = SimulatorNumber
    
    @classmethod
    def CreateAverageSimulationResult(cls, SimulationResults, BanditArmMeanOfMeans, BanditArmStdOfMeans, BanditArmMeanOfStd, BanditArmStdOfStd, BanditArmStats = {}, SimulatorNumber = 1):    
        '''
        Create a single simulation result out of a list of simulation results

        Args:
            SimulationResults (list): The simulation results to make the new instance from.
            BanditArmMeanOfMeans (float): The mean of the mean of any arm of the bandit.
            BanditArmStdOfMeans (float): The std of the mean of any arm of the bandit.
            BanditArmMeanOfStd (float): The mean of the std of any arm of the bandit.
            BanditArmStdOfStd (float): The std of the std of any arm of the bandit.
            BanditArmStats (dict, optional): The full arm stats of the bandit. Defaults to {}.
            SimulatorNumber (int, optional): The number of this simulator. Defaults to 1.

        Returns:
            SimulationResult: The combined average simulation result.
        '''          
        SimulationsRoundRegretAverage = []
        SimulationsTotalRegret = []
        SimulationsRoundPortionBest = []
        SimulationsTotalPortionBest = []
        SimulationsBestArmScore = []
        for SimulationResult in SimulationResults:
            SimulationsRoundRegretAverage.append(SimulationResult.RoundRegretAverage)
            SimulationsTotalRegret.append(SimulationResult.TotalRegret)
            SimulationsRoundPortionBest.append(SimulationResult.RoundPortionBest)
            SimulationsTotalPortionBest.append(SimulationResult.TotalPortionBest)
            SimulationsBestArmScore.append(SimulationResult.BestArmScore)
        return cls(HelperFunctionLib.AverageLists(SimulationsRoundRegretAverage), numpy.mean(SimulationsTotalRegret), 
                                          HelperFunctionLib.AverageLists(SimulationsRoundPortionBest), numpy.mean(SimulationsTotalPortionBest), 
                                          numpy.mean(SimulationsBestArmScore), BanditArmMeanOfMeans, BanditArmStdOfMeans, BanditArmMeanOfStd, BanditArmStdOfStd, BanditArmStats, SimulatorNumber)

    @classmethod
    def ZoomSimulationResult(cls, SimulationResultToZoom, ZoomMin, ZoomMax):    
        '''
        Create a zoomed version of a simulation result (only including the portions between zoom min and max).

        Args:
            SimulationResult (SimulationResult): The result we would like to zoom.
            ZoomMin (float): The portion of the X axis on which to start the zoom.
            ZoomMax (float): The portion of the X axis on which to end the zoom.

        Returns:
            SimulationResult: _description_
        '''            
        assert ZoomMax > ZoomMin    
        
        MinIndex = int(len(SimulationResultToZoom.RoundPortionBest) * ZoomMin)
        MaxIndex = int(len(SimulationResultToZoom.RoundPortionBest) * ZoomMax)
        RoundRegretAverage = SimulationResultToZoom.RoundRegretAverage[MinIndex:MaxIndex]
        RoundPortionBest = SimulationResultToZoom.RoundPortionBest[MinIndex:MaxIndex]
        
        return cls(RoundRegretAverage, SimulationResultToZoom.TotalRegret, RoundPortionBest, SimulationResultToZoom.TotalPortionBest, 
                   SimulationResultToZoom.BestArmScore, SimulationResultToZoom.BanditArmMeanOfMeans, SimulationResultToZoom.BanditArmStdOfMeans, SimulationResultToZoom.BanditArmMeanOfStd, SimulationResultToZoom.BanditArmStdOfStd, SimulationResultToZoom.BanditArmStats, SimulationResultToZoom.SimulatorNumber)