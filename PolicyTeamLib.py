import Logger
import Arm, Bandit, Knowledge, PolicyAgentLib
import math

def HandleArmVetoes(RoundNumber, Agents, Bandit, Percentile=0.9, RequireMoreKnowledge = False, MaxStubbornnessFunnels = 0, MaxAttempts = 5) -> list:
    '''
    Handle finding an arm with arm vetoes and returns the arms that survived the veto process.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        Percentile (float): Percentile value for arm vetoes (default is 0.9).
        RequireMoreKnowledge (bool): Indicates whether more knowledge is required for the agent to veto (default is False).
        MaxStubbornnessFunnels (int): Maximum number of stubbornness funnels allowed (default is 0).
        MaxAttempts (int): Maximum number of attempts before moving on (default is 5).

    Returns:
        list: A list containing arm information for arms that survived the veto process.
    '''
    MaxAttempts = int(MaxAttempts / MaxStubbornnessFunnels) if MaxStubbornnessFunnels != 0 else MaxAttempts
    Attempt = 0
    Funnel = 0
    BusyFinding = True
    BestArms = {}
    RequestedArms = {}
    
    for Agent in Agents:
        BestArms[Agent] = Agent.RecallBestLocallyKnownArm()
    
    while BusyFinding:
        for Agent in Agents:
            # Don't make a new request if something was already accepted
            if not RequestedArms.get(Agent, None):
                RequestedArms[Agent] = Agent.SelectArm(Bandit, RoundNumber)
        for VetoAgent in Agents:
            for Requester, Request in RequestedArms.items():
                # We skip if someone already invalidated the request
                # We only veto if it is not our request, we don't agree with the request, and the request mean is not the highest we know
                if Request and Requester != VetoAgent and Request != RequestedArms[VetoAgent] and Request != BestArms[VetoAgent]:
                    VetoAgentRecall = VetoAgent.RecallEPSpecificArm(Request["Arm"], Percentile)
                    ArmMax = VetoAgentRecall["MeanScore"]
                    # if we need more knowledge than requester set samplesize to that, otherwise set to 1
                    MinSampleSize = Request["SampleSize"] if RequireMoreKnowledge else 1
                    if Request["MeanScore"] > ArmMax and VetoAgentRecall["SampleSize"] > MinSampleSize:
                        RequestedArms[Requester] = None
        # If we all still have requested arms, we can quit otherwise loop again
        Attempt += 1
        BusyFinding = False
        ArmsToValidate = RequestedArms.copy()
        for Requester, Request in ArmsToValidate.items():
            if not Request:
                RequestedArms.pop(Requester)
                if MaxAttempts > Attempt:
                    BusyFinding = True

        # We ran out of attempts and still have no valid arms, see if we can find it with less stubbornness
        if RequestedArms == {} and MaxStubbornnessFunnels < Funnel and not BusyFinding:
            BusyFinding = True
            Attempt = 0
            Funnel += 1
            Percentile = 1 - ((1-Percentile) / 10)
                            
    return RequestedArms

def RandomAgent(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select a random arm from the agent requests.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    SelectedAgent = None
    
    for Agent in Agents:
        RequestedArms[Agent] = Agent.SelectArm(Bandit, RoundNumber)
    SelectedArmIndex = RngGenerator.integers(0,len(RequestedArms))
    ArmDict = list(RequestedArms.values())[SelectedArmIndex]
    
    # If the agent policy had an explore result we might treat it differently, should only be sent by greedy if we don't care
    if not ArmDict.get("Exploit", True):
        SelectedAgent = list(RequestedArms.keys())[SelectedArmIndex]
        ArmDict = PolicyAgentLib.RandomArm(RoundNumber, SelectedAgent, Bandit, SelectedAgent.Rng)

    Score = ArmDict["Arm"].Play()
      
    for Agent in Agents:
        Agent.RememberResult(ArmDict["Arm"], Score)

    return {"Score" : Score, "SelectedMean": ArmDict["Arm"].Mean, "BestArm": ArmDict["Arm"] == Bandit.BestArm}

def RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, Percentile=0.9, RequireMoreKnowledge = False, MaxStubbornnessFunnels = 0, MaxAttempts = 5) -> dict:
    '''
    Select a random arm from the agent requests, but allow unlikely requests to be vetoed.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.
        Percentile (float): Percentile value for arm vetoes (default is 0.9).
        RequireMoreKnowledge (bool): Indicates whether more knowledge is required for the agent to veto (default is False).
        MaxStubbornnessFunnels (int): Maximum number of stubbornness funnels allowed (default is 0).
        MaxAttempts (int): Maximum number of attempts before moving on (default is 5).

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    
    # Ugly way of getting this stat
    if not "AmountOfFailures" in globals():
        global AmountOfFailures
        AmountOfFailures = 0
    
    RequestedArms = HandleArmVetoes(RoundNumber, Agents, Bandit, Percentile, RequireMoreKnowledge, MaxStubbornnessFunnels, MaxAttempts)
    
    if RoundNumber % 500 == 0:
        print(Logger.LoggerPrefix() + " Veto fallbacks used: " + str(AmountOfFailures))
    
    # Fall back on normal random agent if we didn't find anything that didn't get vetoed
    if RequestedArms == {}:
        AmountOfFailures += 1
        return RandomAgent(RoundNumber, Agents, Bandit, RngGenerator)
    
                  
    SelectedArmIndex = RngGenerator.integers(0,len(RequestedArms))
    ArmDict = list(RequestedArms.values())[SelectedArmIndex]

    Score = ArmDict["Arm"].Play()
      
    for Agent in Agents:
        Agent.RememberResult(ArmDict["Arm"], Score)

    return {"Score" : Score, "SelectedMean": ArmDict["Arm"].Mean, "BestArm": ArmDict["Arm"] == Bandit.BestArm}

def RandomVeto90A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.90, MaxAttempts = 1)

def RandomVeto90B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.90, MaxAttempts = 5)

def RandomVeto90C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.90, MaxAttempts = 10)

def RandomVeto90D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.90, MaxAttempts = 15)

def RandomVeto90E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.90, MaxAttempts = 20)

def RandomVeto91A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.91, MaxAttempts = 1)

def RandomVeto91B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.91, MaxAttempts = 5)

def RandomVeto91C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.91, MaxAttempts = 10)

def RandomVeto91D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.91, MaxAttempts = 15)

def RandomVeto91E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.91, MaxAttempts = 20)

def RandomVeto92A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.92, MaxAttempts = 1)

def RandomVeto92B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.92, MaxAttempts = 5)

def RandomVeto92C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.92, MaxAttempts = 10)

def RandomVeto92D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.92, MaxAttempts = 15)

def RandomVeto92E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.92, MaxAttempts = 20)

def RandomVeto93A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.93, MaxAttempts = 1)

def RandomVeto93B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.93, MaxAttempts = 5)

def RandomVeto93C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.93, MaxAttempts = 10)

def RandomVeto93D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.93, MaxAttempts = 15)

def RandomVeto93E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.93, MaxAttempts = 20)

def RandomVeto94A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.94, MaxAttempts = 1)

def RandomVeto94B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.94, MaxAttempts = 5)

def RandomVeto94C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.94, MaxAttempts = 10)

def RandomVeto94D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.94, MaxAttempts = 15)

def RandomVeto94E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.94, MaxAttempts = 20)

def RandomVeto95A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, MaxAttempts = 1)

def RandomVeto95B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, MaxAttempts = 5)

def RandomVeto95C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, MaxAttempts = 10)

def RandomVeto95D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, MaxAttempts = 15)

def RandomVeto95E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, MaxAttempts = 20)

def RandomVeto96A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.96, MaxAttempts = 1)

def RandomVeto96B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.96, MaxAttempts = 5)

def RandomVeto96C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.96, MaxAttempts = 10)

def RandomVeto96D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.96, MaxAttempts = 15)

def RandomVeto96E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.96, MaxAttempts = 20)

def RandomVeto97A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.97, MaxAttempts = 1)

def RandomVeto97B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.97, MaxAttempts = 5)

def RandomVeto97C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.97, MaxAttempts = 10)

def RandomVeto97D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.97, MaxAttempts = 15)

def RandomVeto97E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.97, MaxAttempts = 20)

def RandomVeto98A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.98, MaxAttempts = 1)

def RandomVeto98B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.98, MaxAttempts = 5)

def RandomVeto98C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.98, MaxAttempts = 10)

def RandomVeto98D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.98, MaxAttempts = 15)

def RandomVeto98E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.98, MaxAttempts = 20)

def RandomVeto99A(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.99, MaxAttempts = 1)

def RandomVeto99B(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.99, MaxAttempts = 5)

def RandomVeto99C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.99, MaxAttempts = 10)

def RandomVeto99D(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.99, MaxAttempts = 15)

def RandomVeto99E(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.99, MaxAttempts = 20)

def RandomVetoKnowledgable95C(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return RandomVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, True, MaxAttempts = 10)

def MaxAgent(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest mean from the agent requests.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''           
    BestArmScore = -1
    BestRequest = {}
    SelectedAgent = None
    
    for Agent in Agents:
            RequestedArm = Agent.SelectArm(Bandit, RoundNumber)
            if RequestedArm["MeanScore"] > BestArmScore:
                BestArmScore = RequestedArm["MeanScore"]
                BestRequest = RequestedArm
                SelectedAgent = Agent

    # If the agent policy had an exploit result we might treat it differently
    if not BestRequest.get("Exploit", True):
        BestRequest = PolicyAgentLib.RandomArm(RoundNumber, SelectedAgent, Bandit, SelectedAgent.Rng)
    
    Score = BestRequest["Arm"].Play()
      
    for Agent in Agents:
        Agent.RememberResult(BestRequest["Arm"], Score)

    return {"Score" : Score, "SelectedMean": BestRequest["Arm"].Mean, "BestArm": BestRequest["Arm"] == Bandit.BestArm}

def MaxVeto(RoundNumber, Agents, Bandit, RngGenerator, Percentile=0.9, RequireMoreKnowledge = False, MaxStubbornnessFunnels = 0, MaxAttempts = 5) -> dict:
    '''
    Select the arm with the highest mean from the agent requests after allowing for vetoes.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.
        Percentile (float): Percentile value for arm vetoes (default is 0.9).
        RequireMoreKnowledge (bool): Indicates whether more knowledge is required for the agent to veto (default is False).
        MaxStubbornnessFunnels (int): Maximum number of stubbornness funnels allowed (default is 0).
        MaxAttempts (int): Maximum number of attempts before moving on (default is 5).

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''           
    BestArmScore = -1
    BestRequest = {}
    SelectedAgent = None
    
    # Ugly way of getting this stat
    if not "AmountOfFailures" in globals():
        global AmountOfFailures
        AmountOfFailures = 0
    
    for RequestedArm in HandleArmVetoes(RoundNumber, Agents, Bandit, Percentile, RequireMoreKnowledge, MaxStubbornnessFunnels, MaxAttempts):  
        if RequestedArm.get("MeanScore", -1) > BestArmScore:
            BestArmScore = RequestedArm["MeanScore"]
            BestRequest = RequestedArm
            SelectedAgent = Agent
        
    # Fall back on normal random agent if we didn't find anything that didn't get vetoed
    if BestRequest == {}:
        AmountOfFailures += 1
        print(Logger.LoggerPrefix() + " Occurance: " + str(AmountOfFailures) + " - Failed to find any arms without veto, using fallback.")
        return MaxAgent(RoundNumber, Agents, Bandit, RngGenerator)

    # If the agent policy had an exploit result we might treat it differently
    if not BestRequest.get("Exploit", True):
        BestRequest = PolicyAgentLib.RandomArm(RoundNumber, SelectedAgent, Bandit, SelectedAgent.Rng)
    
    Score = BestRequest["Arm"].Play()
      
    for Agent in Agents:
        Agent.RememberResult(BestRequest["Arm"], Score)

    return {"Score" : Score, "SelectedMean": BestRequest["Arm"].Mean, "BestArm": BestRequest["Arm"] == Bandit.BestArm}

def MaxVeto95(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return MaxVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95)

def MaxVetoKnowledgable95(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    return MaxVeto(RoundNumber, Agents, Bandit, RngGenerator, 0.95, True)

def MaxCombined(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest mean from the agent requests after giving them all combined info.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''           
    BestArmScore = -1
    BestRequest = {}
    SelectedAgent = None
      
    SharedKnowledge = []
    for Agent in Agents:
        for Knowledge in Agent.RecallAllLocallyKnownArms(MinimalSampleSize = 1):
            SharedKnowledge.append(Knowledge)
      
    for Agent in Agents:
        RequestedArm = Agent.SelectArm(Bandit, RoundNumber, SharedKnowledge)
        if RequestedArm["MeanScore"] > BestArmScore:
            BestArmScore = RequestedArm["MeanScore"]
            BestRequest = RequestedArm
            SelectedAgent = Agent
            
    # If the agent policy had an exploit result we might treat it differently
    if not BestRequest.get("Exploit", True):
        BestRequest = PolicyAgentLib.RandomArm(RoundNumber, SelectedAgent, Bandit, SelectedAgent.Rng)

    Score = BestRequest["Arm"].Play()
      
    for Agent in Agents:
        Agent.RememberResult(BestRequest["Arm"], Score)

    return {"Score" : Score, "SelectedMean": BestRequest["Arm"].Mean, "BestArm": BestRequest["Arm"] == Bandit.BestArm}
  
def MaxOptimismKnown(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest mean from the agent requests after giving them partial shared knowledge.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''           
    BestArmScore = -1
    BestRequest = {}
    SelectedAgent = None
      
    SharedKnowledge = []
    for Agent in Agents:
        for Knowledge in Agent.RecallAllLocallyKnownArms(MinimalSampleSize = 1):
            Knowledge["MeanScore"] = 0
            Knowledge["StdScore"] = 0
            Knowledge["SampleSize"] = 1
            SharedKnowledge.append(Knowledge)
      
    for Agent in Agents:
            RequestedArm = Agent.SelectArm(Bandit, RoundNumber, SharedKnowledge)
            if RequestedArm["MeanScore"] > BestArmScore:
                BestArmScore = RequestedArm["MeanScore"]
                BestRequest = RequestedArm
                SelectedAgent = Agent
            
    # If the agent policy had an exploit result we might treat it differently
    if not BestRequest.get("Exploit", True):
        BestRequest = PolicyAgentLib.RandomArm(RoundNumber, SelectedAgent, Bandit, SelectedAgent.Rng)

    Score = BestRequest["Arm"].Play()   
      
    for Agent in Agents:
        Agent.RememberResult(BestRequest["Arm"], Score)

    return {"Score" : Score, "SelectedMean": BestRequest["Arm"].Mean, "BestArm": BestRequest["Arm"] == Bandit.BestArm}
  
def SingleInfoVote(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest votes using only the info the individual agents have (no shared info).

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    Votes = {}  
    HighestVotedArm = {}
    ExploreVotes = 0  
    SelectedExploreAgent = None   

    for Agent in Agents:
        RequestedArm = Agent.SelectArm(Bandit, RoundNumber)
        RequestedArms[Agent] = RequestedArm
    
    HighestVotes = 0
    for Agent, ArmDict in RequestedArms.items():
        AmountOfVotes = Votes.get(ArmDict["Arm"], 0) + 1
        # If the agent policy had an exploit result we might treat it differently
        if not ArmDict.get("Exploit", True):
            ExploreVotes += 1
            SelectedExploreAgent = Agent
        Votes[ArmDict["Arm"]] = AmountOfVotes
        if AmountOfVotes > HighestVotes:
            HighestVotes = AmountOfVotes
            HighestVotedArm = ArmDict
    
    # If the agent policy had an exploit result we might treat it differently
    if ExploreVotes > (len(Agents) / 2) :
        HighestVotedArm = PolicyAgentLib.RandomArm(RoundNumber, SelectedExploreAgent, Bandit, SelectedExploreAgent.Rng)
    
    Score = HighestVotedArm["Arm"].Play()
    
    for Agent in Agents:
        Agent.RememberResult(HighestVotedArm["Arm"], Score)
    
    return {"Score" : Score, "SelectedMean": HighestVotedArm["Arm"].Mean, "BestArm": HighestVotedArm["Arm"] == Bandit.BestArm}

def HighestInfoVote(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest votes using individual agent information and everyone's highest mean arm.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    Votes = {}
    SharedKnowledge = []  
    HighestVotedArm = {}
    ExploreVotes = 0  
    SelectedExploreAgent = None
             

    for Agent in Agents:
        SharedKnowledge.append(PolicyAgentLib.HighestMeanArm(RoundNumber, Agent, Bandit, Agent.Rng))      
          
    for Agent in Agents:      
        RequestedArm = Agent.SelectArm(Bandit, RoundNumber, SharedKnowledge)
        RequestedArms[Agent] = RequestedArm
    
    HighestVotes = 0
    for ArmDict in list(RequestedArms.values()):
        AmountOfVotes = Votes.get(ArmDict["Arm"], 0) + 1
        # If the agent policy had an exploit result we might treat it differently
        if not ArmDict.get("Exploit", True):
            ExploreVotes += 1
            SelectedExploreAgent = Agent
        Votes[ArmDict["Arm"]] = AmountOfVotes
        if AmountOfVotes > HighestVotes:
            HighestVotes = AmountOfVotes
            HighestVotedArm = ArmDict
    
    # If the agent policy had an exploit result we might treat it differently
    if ExploreVotes > (len(Agents) / 2) :
        HighestVotedArm = PolicyAgentLib.RandomArm(RoundNumber, SelectedExploreAgent, Bandit, SelectedExploreAgent.Rng)
    
    Score = HighestVotedArm["Arm"].Play()
    
    for Agent in Agents:
        Agent.RememberResult(HighestVotedArm["Arm"], Score)
    
    return {"Score" : Score, "SelectedMean": HighestVotedArm["Arm"].Mean, "BestArm": HighestVotedArm["Arm"] == Bandit.BestArm}

def HighestCertaintyInfoVote(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest votes using individual agent information and everyone's arm with the highest certainty.

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    Votes = {}
    SharedKnowledge = []  
    HighestVotedArm = {}
    ExploreVotes = 0  
    SelectedExploreAgent = None          

    for Agent in Agents:
        SharedKnowledge.append(PolicyAgentLib.HighestCertaintyArm(RoundNumber, Agent, Bandit, Agent.Rng))      
          
    for Agent in Agents:      
        RequestedArm = Agent.SelectArm(Bandit, RoundNumber, SharedKnowledge)
        RequestedArms[Agent] = RequestedArm
    
    HighestVotes = 0
    for ArmDict in list(RequestedArms.values()):
        AmountOfVotes = Votes.get(ArmDict["Arm"], 0) + 1
        # If the agent policy had an exploit result we might treat it differently
        if not ArmDict.get("Exploit", True):
            ExploreVotes += 1
            SelectedExploreAgent = Agent
        Votes[ArmDict["Arm"]] = AmountOfVotes
        if AmountOfVotes > HighestVotes:
            HighestVotes = AmountOfVotes
            HighestVotedArm = ArmDict
    
    # If the agent policy had an exploit result we might treat it differently
    if ExploreVotes > (len(Agents) / 2) :
        HighestVotedArm = PolicyAgentLib.RandomArm(RoundNumber, SelectedExploreAgent, Bandit, SelectedExploreAgent.Rng)
    
    Score = HighestVotedArm["Arm"].Play()
    
    for Agent in Agents:
        Agent.RememberResult(HighestVotedArm["Arm"], Score)
    
    return {"Score" : Score, "SelectedMean": HighestVotedArm["Arm"].Mean, "BestArm": HighestVotedArm["Arm"] == Bandit.BestArm}

def OptimismInfoVote(RoundNumber, Agents, Bandit, RngGenerator) -> dict:
    '''
    Select the arm with the highest votes using individual agent information and shared knowledge about which arms are new (without their means).

    Args:
        RoundNumber (int): The current round number.
        Agents (list): List of all the agents that are playing this round.
        Bandit (Bandit): Reference to the bandit on which we are playing.
        RngGenerator (default_rng): Generator to use for random numbers.

    Returns:
        dict: The result of the play, including the score, selected mean, and whether the best arm was chosen.
    '''   
    RequestedArms = {}
    Votes = {}
    SharedKnowledge = []  
    HighestVotedArm = {}
    ExploreVotes = 0  
    SelectedExploreAgent = None         

    for Agent in Agents:
        for Knowledge in Agent.RecallAllLocallyKnownArms(MinimalSampleSize = 1):
            Knowledge["MeanScore"] = 0
            Knowledge["StdScore"] = 0
            Knowledge["SampleSize"] = 1
            SharedKnowledge.append(Knowledge)      
          
    for Agent in Agents:      
        RequestedArm = Agent.SelectArm(Bandit, RoundNumber, SharedKnowledge)
        RequestedArms[Agent] = RequestedArm
    
    HighestVotes = 0
    for ArmDict in list(RequestedArms.values()):
        AmountOfVotes = Votes.get(ArmDict["Arm"], 0) + 1
        # If the agent policy had an exploit result we might treat it differently
        if not ArmDict.get("Exploit", True):
            ExploreVotes += 1
            SelectedExploreAgent = Agent
        Votes[ArmDict["Arm"]] = AmountOfVotes
        if AmountOfVotes > HighestVotes:
            HighestVotes = AmountOfVotes
            HighestVotedArm = ArmDict
    
    # If the agent policy had an exploit result we might treat it differently
    if ExploreVotes > (len(Agents) / 2) :
        HighestVotedArm = PolicyAgentLib.RandomArm(RoundNumber, SelectedExploreAgent, Bandit, SelectedExploreAgent.Rng)
    
    Score = HighestVotedArm["Arm"].Play()
    
    for Agent in Agents:
        Agent.RememberResult(HighestVotedArm["Arm"], Score)
    
    return {"Score" : Score, "SelectedMean": HighestVotedArm["Arm"].Mean, "BestArm": HighestVotedArm["Arm"] == Bandit.BestArm}

# A Combined vote makes no sense as all agents should have the same info and therefore vote (nearly) the same
PolicyDict = {
    "Random" : RandomAgent,
    "RandomV90A" : RandomVeto90A,
    "RandomV90B" : RandomVeto90B,
    "RandomV90C" : RandomVeto90C,
    "RandomV90D" : RandomVeto90D,
    "RandomV90E" : RandomVeto90E,
    "RandomV91A" : RandomVeto91A,
    "RandomV91B" : RandomVeto91B,
    "RandomV91C" : RandomVeto91C,
    "RandomV91D" : RandomVeto91D,
    "RandomV91E" : RandomVeto91E,
    "RandomV92A" : RandomVeto92A,
    "RandomV92B" : RandomVeto92B,
    "RandomV92C" : RandomVeto92C,
    "RandomV92D" : RandomVeto92D,
    "RandomV92E" : RandomVeto92E,
    "RandomV93A" : RandomVeto93A,
    "RandomV93B" : RandomVeto93B,
    "RandomV93C" : RandomVeto93C,
    "RandomV93D" : RandomVeto93D,
    "RandomV93E" : RandomVeto93E,
    "RandomV94A" : RandomVeto94A,
    "RandomV94B" : RandomVeto94B,
    "RandomV94C" : RandomVeto94C,
    "RandomV94D" : RandomVeto94D,
    "RandomV94E" : RandomVeto94E,
    "RandomV95A" : RandomVeto95A,
    "RandomV95B" : RandomVeto95B,
    "RandomV95C" : RandomVeto95C,
    "RandomV95D" : RandomVeto95D,
    "RandomV95E" : RandomVeto95E,
    "RandomV96A" : RandomVeto96A,
    "RandomV96B" : RandomVeto96B,
    "RandomV96C" : RandomVeto96C,
    "RandomV96D" : RandomVeto96D,
    "RandomV96E" : RandomVeto96E,
    "RandomV97A" : RandomVeto97A,
    "RandomV97B" : RandomVeto97B,
    "RandomV97C" : RandomVeto97C,
    "RandomV97D" : RandomVeto97D,
    "RandomV97E" : RandomVeto97E,
    "RandomV98A" : RandomVeto98A,
    "RandomV98B" : RandomVeto98B,
    "RandomV98C" : RandomVeto98C,
    "RandomV98D" : RandomVeto98D,
    "RandomV98E" : RandomVeto98E,
    "RandomV99A" : RandomVeto99A,
    "RandomV99B" : RandomVeto99B,
    "RandomV99C" : RandomVeto99C,
    "RandomV99D" : RandomVeto99D,
    "RandomV99E" : RandomVeto99E,
    "RandomVK95C" : RandomVetoKnowledgable95C,
    "MaxA" : MaxAgent,
    "MaxC" : MaxCombined,
    "MaxO" : MaxOptimismKnown,
    "MaxV95" : MaxVeto95,
    "MaxVK95" : MaxVetoKnowledgable95,
    "VoteS" : SingleInfoVote,
    "VoteH" : HighestInfoVote,
    "VoteC" : HighestCertaintyInfoVote,
    "VoteO" : OptimismInfoVote
}